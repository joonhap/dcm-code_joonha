/* file odemod_rv.c */
// revamped version of odemod.c where vaccination increaes according to a certain increase rate and stops after a specified years after the last polio case.
// include libraries
#include <stdio.h>
#include <R.h> // needed for connecting to R
#include <Rinternals.h>
#include <Rdefines.h>
#include <R_ext/Rdynload.h>
#include <math.h> // needed for simple math functions, such as sqrt

// define parameters (globally available in this file)
static double parms[17];
#define n 40
#define StartT parms[0]
#define m parms[1]
#define VT parms[2]
#define VaccRInc parms[3]
#define VaccLvlYr parms[4]
#define PopSize parms[5]
#define ErLv parms[6]
#define InfPerAFP parms[7]
#define gW1 parms[8]
#define RelContOtW parms[9]
#define RecOtWratio parms[10]
#define con parms[11]
#define tp parms[12]
#define WnRt parms[13]
#define SucRt1ratio parms[14]
#define RelContRt1 parms[15]
#define Rec1tRratio parms[16]
#define gO1 gW1*RecOtWratio
#define gWR gW1/Rec1tRratio
#define gOR gW1/Rec1tRratio*RecOtWratio

/* Initializer */
// needed to import parms from R (no need to modify this function, except for `Nparms')
void initmod(void (*odeparms)(int *, double *))
{
  int Nparms = 17; // the number of parameters received
  odeparms(&Nparms, parms);
}

/* Derivatives */
// defines the model differential equations
// this function is iteratively called to compute the derivatives at give state values
void derivs (int *neq, double *t, double *y, double *ydot,
double *yout, int *ip)
// arguments: neq--number of equations (internally handled), t--current time, y--the current state vector (input), ydot--the vector of derivatives (output), yout--the vector of additional return values (output), ip--number of integer parameters (for internal use)
{
  // y is actually the `pointer' to the physical memory corresponding to the first element of the input state vector. First (n+1) of them correspond to S, second (n+1) correspond to WPV1, etc. (S, WPV1, WPVR, OPV1, OPVR, R1, R2). After 7*(n+1) entries, there follow CumFrstWInf, CumFrstFrFrst, CumFrstFrRe, CumReFrFrst, CumReFrRe.
  double* S = y;
  double* WPV1 = y+n+1;
  double* WPVR = y+2*(n+1);
  double* OPV1 = y+3*(n+1);
  double* OPVR = y+4*(n+1);
  double* R1 = y+5*(n+1);
  double* R2 = y+6*(n+1);
  double* CumFrstWInf = y+7*(n+1);
  double* CumFrstFrFrst = y+7*(n+1)+1;
  double* CumFrstFrRe = y+7*(n+1)+2;
  double* CumReFrFrst = y+7*(n+1)+3;
  double* CumReFrRe = y+7*(n+1)+4;
  double* EradStatus = y+7*(n+1)+5;

  // define VaccRtbe as a function of t. 
  double VaccRtbe = (t[0]>VT) * fmin( (t[0]-VT)*VaccRInc, (VaccLvlYr - VT)*VaccRInc );

  double totS = 0.0;
  double totWPV1 = 0.0;
  double totWPVR = 0.0;
  double totOPV1 = 0.0;
  double totOPVR = 0.0;
  double totR1 = 0.0;
  double totR2 = 0.0;
  for (int i=0; i<n+1; i++) {
    totS += S[i];
    totWPV1 += WPV1[i];
    totWPVR += WPVR[i];
    totOPV1 += OPV1[i];
    totOPVR += OPVR[i];
    totR1 += R1[i];
    totR2 += R2[i];
  }
  double totPop = totS + totWPV1 + totWPVR + totOPV1 + totOPVR + totR1 + totR2;
  double LmbdaW = (totWPV1 + totWPVR * RelContRt1) / totPop ;
  double LmbdaO = (totOPV1 + totOPVR * RelContRt1) * RelContOtW / totPop;

  // ydot is the pointer to the physical memory corresponding to the vector of derivatives (to be precise, the first element of it). Once the values in these momory slots are modified, they can be read in R directly. This is how the computed derivatives are passed to R.
  double* dS = ydot;
  double* dWPV1 = ydot+n+1;
  double* dWPVR = ydot+2*(n+1);
  double* dOPV1 = ydot+3*(n+1);
  double* dOPVR = ydot+4*(n+1);
  double* dR1 = ydot+5*(n+1);
  double* dR2 = ydot+6*(n+1);
  double* dCumFrstWInf = ydot+7*(n+1);
  double* dCumFrstFrFrst = ydot+7*(n+1)+1;
  double* dCumFrstFrRe = ydot+7*(n+1)+2;
  double* dCumReFrFrst = ydot+7*(n+1)+3;
  double* dCumReFrRe = ydot+7*(n+1)+4;
  double* dEradStatus = ydot+7*(n+1)+5;

  // Derivatives
  dS[0] = m*totPop - S[0]*con*tp*(LmbdaW +LmbdaO) - S[0] * ((n / 5.0) +m +VaccRtbe);
  for(int i=1; i<n; i++)
    dS[i] = S[i-1] * (n/5.0) - S[i]*con*tp*(LmbdaW +LmbdaO) - S[i] * ((n/5.0)+m+VaccRtbe);
  dS[n] = S[n-1] * (n/5.0) - S[n]*con*tp*(LmbdaW +LmbdaO) - S[n] * m;
  for(int i=0; i<n+1; i++) {
    dWPV1[i] = ( (i>0)*WPV1[i-1]*n/5.0 + S[i]*con*tp*LmbdaW - WPV1[i]*((m + gW1) + (i<n)* n/5.0) ) * (1-EradStatus[0]) ; // after WPV is eradicated, the prevalence stays at 0.
    dOPV1[i] = (i>0)*OPV1[i-1]*n/5.0 + (i<n)* S[i] * VaccRtbe + S[i]*con*tp*LmbdaO - OPV1[i]*(m+(i < n)* n/5.0 + gO1);
    dR1[i] = (i >0)* R1[i-1]*n/5.0 + WPV1[i] * gW1 + WPVR[i] * gWR + OPV1[i] * gO1 + OPVR[i] * gOR  - R1[i] * WnRt - R1[i] * ((i < n)* n/5.0 + m);
    dR2[i] = (i >0)* R2[i-1]*n/5.0 + R1[i]*WnRt - R2[i]*con*SucRt1ratio*tp*(LmbdaW + LmbdaO) - R2[i] * ((i < n)*n/5.0 + (i < n)*VaccRtbe*SucRt1ratio + m);
    dWPVR[i] = ( (i >0)*WPVR[i-1]*n/5.0 + R2[i]*con*tp*SucRt1ratio*LmbdaW - WPVR[i]*(m + (i<n)* n/5.0 + gWR) ) * (1-EradStatus[0]); // after WPV is eradicated, the prevalence stays at 0.
    dOPVR[i] = (i >0)*OPVR[i-1]*n/5.0 + R2[i]*(con*tp*SucRt1ratio*LmbdaO + (i < n)*VaccRtbe*SucRt1ratio) - OPVR[i]*(m + (i < n)*n/5.0+gOR);
  }

  // Cumulative incidences (NOT for one-year span, but the total from t=0.)
  dCumFrstWInf[0] = totS*con*tp*LmbdaW;
  dCumFrstFrRe[0] = totS*con*tp*totWPVR* RelContRt1 / totPop;
  dCumReFrRe[0] = totR2*SucRt1ratio*con*tp*totWPVR* RelContRt1 / totPop;
  dCumFrstFrFrst[0] = totS*con*tp*totWPV1 / totPop;
  dCumReFrFrst[0] = totR2*SucRt1ratio*con*tp*totWPV1 / totPop;
  dEradStatus[0] = 0.0;
}

/* WPV reintroduction event */
void reintro(int *nstate, double *t, double *y) 
{
  double* WPVR = y+2*(n+1);
  double* R2 = y+6*(n+1);
  WPVR[n] += 1.0;
  R2[n] -= 1.0;
}


/* eradication event */
void eradication(int *nstate, double *t, double *y)
{
  *(y+7*(n+1)+5) = 1.0; // EradStatus
  double* WPV1 = y+n+1;
  double* WPVR = y+2*(n+1);
  double* R1 = y+5*(n+1);
  for(int i = 0; i < n+1; i++) {
    R1[i] += (WPV1[i] + WPVR[i]);
    WPV1[i] = 0.0;
    WPVR[i] = 0.0;
  }
}

/* root functions */

/* eradication trigger */
// when totWPV equals ErLv, we will set WPV prevalence and its growth rate to 0.
void eradroot(int *neq, double *t, double *y, int *ng, double *gout, double *out, int *ip)
{
  double totWPV = 0.0;
  for (int i=0; i<2*n+2; i++) {
    totWPV += y[n+1+i];
  }
  gout[0] = totWPV - ErLv;
}


/* END file odemod.c */
