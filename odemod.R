## load R library deSolve
library(deSolve)

## compile "odemod.c" using R mediated C compiler
## the following line uses system command to create "odemod.dll" file (dll: dynamic link library) (for linux, it will be "odemod.so")
system("R CMD SHLIB odemod.c")
## load the "odemod.dll" (which is called the shared object) into R
## .Platform$dynlib.ext is just a character string which is either ".dll" for Windows or ".so" for Linux, depending on the platform being used.
dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))

## the number of childhood age compartments (`n' in the `initmode' function in the C code should be manually set to the same value)
n = 40

## define a sequence of time points at which we want the output
## (The interval length corresponds to "DTOUT" in BM. The step size "DT" is automatically chosen by the `ode' function to generate accurate solution with high speed)
StartT = -150
maxT = 100
times <- seq(from=StartT, to=maxT, by=.1)

## define a parameter vector to be passed to "ode" function
m = 0.02
VT = 0
VaccRbe = 1
VRampT = 18
PopSize = 1e6
ErLv = 1
InfPerAFP = 200
FolYrs = 14
gW1 = 13
RecOtWratio = 2
con = 260
tp = 0.5
RelContOtW = 0.5
AFPLv = InfPerAFP
WnRt = 0.02
SucRt1ratio = 0.375^(1/3)
RelContRt1 = SucRt1ratio
Rec1tRratio = SucRt1ratio
CntDurFrct = RelContRt1 * Rec1tRratio
gO1 = gW1*RecOtWratio
gWR = gW1/Rec1tRratio
gOR = gW1/Rec1tRratio*RecOtWratio
# define parameter vector (syntax: X = X -- first X is the name of the component in the vector, second X is the variable defined above)
parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)


# set up an inital condition
# initdata contains the initial compartment sizes (in fraction with respect to the total size) as defined in the BM source file
# in the order of (S, WPV1, WPVR, OPV1, OPVR, R1, R2), and each category is subdivided into n+1 age subgroups (e.g., S[0], S[1], ..., S[n+1])
# read the data file using the function read.table, which create a data structure called `data.frame'. Since a data.frame is a subclass of `list', and what we want is a `vector', we convert the data using unlist command
initproportion <- unlist(read.table(file="initdata",header=T)[1,])

# initial compartment sizes are the product of initproportion and PopSize.
init <- initproportion * parms["PopSize"]

# the last 6th to 2nd components of init vector are CumFrstWInf, CumFrstFrFrst, CumFrstFrRe, CumReFrFrst, CumReFrRe, all initialized at 0 (there values are the sums from the beginning of the time, NOT one-year total). the last component is an indicator (0 or 1) of eradication status (0= eradicated, 1= not eradicated). This component is used to compute the R_eff for WPV under the circumstances that WPV is completely eradicated.
init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)

# solve the ODE using the complied file ("odemod.dll" or "odemod.so") of the C code "odemod.c"
out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
#out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod", rootfunc="eradroot", nroot=1, events=list(func="eradication", root=TRUE))
# compute derived variables 
source('derivedVariables.R')


pos <- (-StartT/(times[2]-times[1])+1):length(times) #indices for positive time points

par(mfrow=c(2,1)); par(mar=c(2,3,2,1))
# Example plot: totWPV against time, with vertical bars
plot(times[pos], totWPV[pos], type='l', main=paste('R0 = ',con*tp/gW1,', wr = ',WnRt,', wd^3 = ',SucRt1ratio^3,', VaccRate = ',VaccRbe,', VRampT = ',VRampT, sep=''))
points(times[pos], totWPV1[pos], type='l', lty=2)
abline(v=FrstAFPTm, col='blue') # draw a vertical line at FrstAFPTm
abline(v=ScndAFPTm, col='red') # draw a vertical line at ScndAFPTm
abline(v=Erad3Tm, col='green') # draw a vertical line at Erad3Tm
abline(v=VT+VRampT, col='yellow', lty=2) # draw a vertical line at the end of ramp-up
legend('topright', legend=c('First AFP time', 'Second AFP time', 'Eradication time (3)'), col=c('blue','red','green'), lty=1) 
legend('topright', inset=c(0,0.5), legend=c('Total WPV', 'Total WPV1'), lty=c(1,2))
mtext('prevalence', side=2, line=2)
#mtext('Time (yr)', side=1, line=2)
#dev.off()

### Simple R0 plot
#out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod", events=list(func="eradication", time=VT+VRampT))
#out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
#source("derivedVariables.R")
par(mar=c(3,3,1,1))
plot(times[pos], ReffTot[pos], type='l',ylim=c(0,1.3), xlab='',ylab='')
points(times[pos], ReffRe[pos], type='l', col='blue')
points(times[pos], ReffFrst[pos], type='l', col='green')
legend("topright", legend=c(expression(r^{Tot}), expression(r^{First}), expression(r^{Re})), col=c("black", "green", "blue"), lty=1)
mtext("Time", side=1, line=2)
mtext("Instantaneous reproduction number", side=2, line=2)
abline(h=1, lty=3)

#dev.copy(device=pdf, paste('plots/R0 = ',con*tp/gW1,', wr = ',WnRt,', wd^3 = ',SucRt1ratio^3,', VaccRate = ',VaccRbe,', VRampT = ',VRampT,'.pdf', sep=''))
#dev.off()
