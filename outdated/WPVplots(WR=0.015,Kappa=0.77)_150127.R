library(deSolve)
system("R CMD SHLIB odemod.c")
dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))

n = 40
StartT = -50
maxT = 100
times <- seq(from=StartT, to=maxT, by=.1)
m = 0.02
VT = 0
VaccRbe = 0.35
VRampT = 15
PopSize = 1e6
ErLv = 1
InfPerAFP = 100
FolYrs = 15
gW1 = 13
RecOtWratio = 2
con = 390
tp = 0.5
RelContOtW = 0.5
AFPLv = InfPerAFP
WnRt = 0.03
SucRt1ratio = 0.6
RelContRt1 = SucRt1ratio
Rec1tRratio = SucRt1ratio
CntDurFrct = RelContRt1 * Rec1tRratio
gO1 = gW1*RecOtWratio
gWR = gW1/Rec1tRratio
gOR = gW1/Rec1tRratio*RecOtWratio
parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)


initproportion <- unlist(read.table(file="initdata",header=T)[1,])
init <- initproportion * parms["PopSize"]
init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)

pos <- (-StartT/(times[2]-times[1])+1):length(times) #indices for positive time points

pdf("plots/WPVplots(WR=0.03,Kappa=0.6)_150502.pdf",width=10,height=7)
par(mfrow=c(2,2))
par(mar=c(2,2.3,2,1))
par(oma=c(2.1, 1.1, 0, 0))
par(lwd=1.3, cex=1.2)

## no case elimination
parms["VaccRbe"] <- 0.75
out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
source("derivedVariables.R")
plot(times[pos], totWPV[pos], type='l', xlab=NA, ylab=NA, ylim=c(0, 2.6e3))
mtext("Total WPV infection prevalence", side=2, line=2)
mtext("(a) Vaccination rate: 0.75 / yr", side=3, line= 0.5)


## silent circulation
parms["VaccRbe"] <- 1.2
out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
source("derivedVariables.R")
plot(times[pos], totWPV[pos], type='l', xlab=NA, ylab=NA, ylim=c(0, 2.6e3))#,xlim=c(140,200))
mtext("(b) Vaccination rate: 1.2 / yr", side=3, line= 0.5, cex=1.2)
abline(v=FrstAFPTm, col='blue', lwd=1.5) # draw a vertical line at FrstAFPTm
segments(x0=ScndAFPTm, y0=-100, y1=1720, col='red', lwd=1.5) # draw a vertical line at ScndAFPTm
legend('topright', legend=c('Last AFP before silence', 'Resurgent AFP'), col=c('blue','red'), lty=1, lwd=1.5) # add a legend to the plot


## unstable eradication
parms["VaccRbe"] <- 1.3
out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod", rootfunc="eradroot", nroot=1, events=list(func="eradication", root=TRUE))
source("derivedVariables.R")
plot(times[pos], totWPV[pos], type='l', xlab=NA, ylab=NA, ylim=c(0, 2.6e3))#,xlim=c(140,200))
out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
source("derivedVariables.R")
points(times[pos], totWPV[pos], type='l', lty=2, xlab=NA, ylab=NA)#,xlim=c(140,200))
mtext("Total WPV infection prevalence", side=2, line=2)
mtext("Time (years)", side=1, line=2)
mtext("(c) Vaccination rate: 1.3 / yr", side=3, line= 0.5, cex=1.2)
abline(v=FrstAFPTm, col='blue', lwd=1.5) # draw a vertical line at FrstAFPTm
segments(x0=ScndAFPTm, y0=-100, y1=1510, col='red', lty=2, lwd=1.5) # draw a vertical line at Resurrgent AFPTm
abline(v=Erad3Tm, col='green', lwd=1.5) # draw a vertical line at Erad3Tm
legend('topright', legend=c('Last AFP before eradication', 'Local eradication', 'Resurgent AFP'), col=c('blue','green','red'), lty=c(1,1,2), lwd=1.5) # add a legend to the plot


## stable eradication
parms["VaccRbe"] <- 1.8
out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")#, rootfunc="eradroot", nroot=1, events=list(func="eradication", root=TRUE))
source("derivedVariables.R")
plot(times[pos], totWPV[pos], type='l', xlab=NA, ylab=NA, ylim=c(0, 2.6e3))#,xlim=c(140,200))
mtext("Time (years)", side=1, line=2)
mtext("(d) Vaccination rate: 1.8 / yr", side=3, line= 0.5, cex=1.2)
abline(v=FrstAFPTm, col='blue', lwd=1.5)# draw a vertical line at FrstAFPTm
abline(v=Erad3Tm, col='green', lwd=1.5) # draw a vertical line at Erad3Tm
legend('topright', legend=c('Last AFP before eradication', 'Eradication'), col=c('blue','green'), lty=1, lwd=1.5) # add a legend to the plot



dev.off()
