require(plot3D)

R0 = 15
AverageAgeAtI1 = 2.5
InfPerAFP = 200
OvsW = 0.25

con = R0 * 13 / 0.5

## generate aligned plots
load(paste('RData/wr_wd_maofi_',AverageAgeAtI1,'_con_390.RData',sep=''))
##wr.vec <- c(0.15, 0.06, 0.03, 0.02)
##wd.vec <- c(0.36, 0.46, 0.60, 0.72)

VRampTgrid <- seq(from= 0.4, to= 20, by=0.4); nvrt <- length(VRampTgrid)
VaccRbegrid <- seq(from= 0.1, to= 2.5, by=0.02); nvrb <- length(VaccRbegrid)

i.ptd <- 3
wr <- wr.vec[i.ptd]; wd <- wd.vec[i.ptd]
load(paste("RData/type_duration_con_", con, "_wr_", wr, "_wd_", wd,"_AvAgeFI_",AverageAgeAtI1,"_InfPerAFP_",InfPerAFP,"_OvsW_",OvsW, "_150522.RData",sep=""))
#wr <- 0.03; wd <- 0.6
load(paste("RData/type_duration_con_390_wr_",wr,"_wd_",wd,"_150328.RData", sep=""))

## mark VaccRbe values at which either Silent duration either starts or ceases to exceed 3 yrs.
Silent3yrStartEnd <- data.frame(NULL)
for(j.ptd in 1:nvrt) {
    Silent3yrStart <- 0; Silent3yrEnd <- 0
    for(k.ptd in 1:nvrb) {
        if(Silent3yrStart == 0 & CombinedResult[(j.ptd-1)*nvrb + k.ptd,"Silent3Dur"] > 3)
            Silent3yrStart <- k.ptd
        if(Silent3yrStart != 0 & k.ptd > Silent3yrStart & CombinedResult[(j.ptd-1)*nvrb + k.ptd,"Silent3Dur"] > 3)
            Silent3yrEnd <- k.ptd
    }
    if (Silent3yrStart != 0)
        Silent3yrStartEnd <- rbind(Silent3yrStartEnd, c(VaccRbegrid[Silent3yrStart], VRampTgrid[j.ptd]))
    if (Silent3yrEnd != 0)
        Silent3yrStartEnd <- rbind(Silent3yrStartEnd, c(VaccRbegrid[Silent3yrEnd], VRampTgrid[j.ptd]))
}
colnames(Silent3yrStartEnd) <- c("VaccRbe", "VRampT")

## stable eradication is marked in green
##StableErad <- sapply(1:dim(CombinedResult)[1], function(x) all(CombinedResult[x:(((x-1)%/%nvrb+1)*nvrb),"StableErad"]==1)) ## since the simulation stops at finite time, in some cases (though very rare) unstable eradication can be marked as stable. So we need to check that all higher vaccination rates result in stable eradication.
CombinedResult[,"Silent3Type"] <- ifelse(CombinedResult[,"StableErad"]==1 & CombinedResult[,"Silent3Type"]==1, 4, CombinedResult[,"Silent3Type"])
## we no longer distinguish the points where no second AFP nor eradication happens until simulation end time.
## instead of purple (as done previously), mark it in red
CombinedResult[,"Silent3Type"] <- ifelse(CombinedResult[,"Silent3Type"]==3, 2, CombinedResult[,"Silent3Type"])        

CombinedResult <- as.data.frame(CombinedResult)
## Eradication status / Silent circulation duration plot ###
## plot of eradication status and the duration of silent circulation
## the color is determined by the 'Silent3Type' column, and the intensity by the 'Silent3Dur'.

pdf(paste("plots/type_duration_3d_con_", con, "_wr_", wr, "_wd_", wd,"_AvAgeFI_",AverageAgeAtI1,"_InfPerAFP_",InfPerAFP,"_OvsW_",OvsW, "_150618.pdf",sep=""))
z=matrix(pmin(8,pmax(0,CombinedResult$Silent3Dur)),nrow=nvrb)
zlim=c(-8,8)
par(mar=c(2,2.8,1,1))
with(CombinedResult,
     {
         ribbon3D(x=VaccRbegrid, y=VRampTgrid, z=z, xlab="Vaccination rate (/yr)", ylab="Ramp up time (yr)", zlab="Length of silent circulation (yr)", colvar=-1/(z+1), phi=20, expand=0.7, d=2, resfac=2, bty="b2",  zlim=zlim, colkey=list(at=-1/(c(0,0.5,1,2,3,5)+1), labels=c("0","0.5","1","2","3","5"), side=1, length=0.75, cex.axis=1.2), clim=-1/(c(0,8)+1), cex.lab=1.5, ticktype="detailed")
         contour3D(x=VaccRbegrid, y=VRampTgrid, z="zmin", add=TRUE, colvar=-1/(z+1), col="grey", lwd=1, levels=-1/(c(0,0.5,1,2,5)+1), colkey=FALSE, plot=TRUE)
         contour3D(x=VaccRbegrid, y=VRampTgrid, z=0.5, addbox=FALSE, add=TRUE, colvar=-1/(z+1), col="black", lwd=1, levels=-1/(c(0.5)+1), colkey=FALSE, plot=TRUE)
         contour3D(x=VaccRbegrid, y=VRampTgrid, z=1, addbox=FALSE, add=TRUE, colvar=-1/(z+1), col="black", lwd=1, levels=-1/(c(1)+1), colkey=FALSE, plot=TRUE)
         contour3D(x=VaccRbegrid, y=VRampTgrid, z=2, addbox=FALSE, add=TRUE, colvar=-1/(z+1), col="black", lwd=1, levels=-1/(c(2)+1), colkey=FALSE, plot=TRUE)
         contour3D(x=VaccRbegrid, y=VRampTgrid, z=5, addbox=FALSE, add=TRUE, colvar=-1/(z+1), col="black", lwd=1, levels=-1/(c(5)+1), colkey=FALSE, plot=TRUE)
         contour3D(x=VaccRbegrid, y=VRampTgrid, z="zmin", add=TRUE, colvar=-1/(z+1), col="black", lwd=2, levels=-1/(c(3)+1), colkey=FALSE, plot=TRUE)
         contour3D(x=VaccRbegrid, y=VRampTgrid, z=3, addbox=FALSE, add=TRUE, colvar=-1/(z+1), col="black", lwd=4, levels=-1/(c(3)+1), colkey=FALSE, plot=TRUE)
     }
     )
dev.off()

