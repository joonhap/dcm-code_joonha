## parameters
wr.vec <- c(0.015,0.05,0.1,0.3,0.7)
wd.vec <- c(0.77,0.6,0.5,0.4,0.3)
vr.vec <- c(0.3, 0.6, 0.9, 1.2)

library(deSolve)
#system("R CMD SHLIB odemod.c")
dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))
n = 40
StartT = -40
maxT = 110
times <- seq(from=StartT, to=maxT, by=.1)

m = 0.02
VT = 10
VaccRbe = 0.3
VRampT = 14
PopSize = 1e6
ErLv = 1
InfPerAFP = 100
FolYrs = 15
gW1 = 13
RecOtWratio = 2
con = 390
tp = 0.5
RelContOtW = 0.5
AFPLv = InfPerAFP
WnRt = 0.1
SucRt1ratio = 0.6
RelContRt1 = SucRt1ratio
Rec1tRratio = SucRt1ratio
CntDurFrct = RelContRt1 * Rec1tRratio
gO1 = gW1*RecOtWratio
gWR = gW1/Rec1tRratio
gOR = gW1/Rec1tRratio*RecOtWratio
parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)

initproportion <- unlist(read.table(file="initdata",header=T)[1,])
init <- initproportion * parms["PopSize"]
init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)

library(doParallel)
cl <- makeCluster(length(vr.vec))
registerDoParallel(cl)

Result <- foreach(k.piad = 1:length(vr.vec), .packages='deSolve', .combine=rbind) %dopar% {
    VaccRbe <- vr.vec[k.piad]
    dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))
    n = 40
    StartT = -40
    maxT = 110
    times <- seq(from=StartT, to=maxT, by=.1)
    
    m = 0.02
    VT = 10
    VRampT = 14
    PopSize = 1e6
    ErLv = 1
    InfPerAFP = 100
    FolYrs = 15
    gW1 = 13
    RecOtWratio = 2
    con = 390
    tp = 0.5
    RelContOtW = 0.5
    AFPLv = InfPerAFP
    WnRt = 0.1
    SucRt1ratio = 0.6
    RelContRt1 = SucRt1ratio
    Rec1tRratio = SucRt1ratio
    CntDurFrct = RelContRt1 * Rec1tRratio
    gO1 = gW1*RecOtWratio
    gWR = gW1/Rec1tRratio
    gOR = gW1/Rec1tRratio*RecOtWratio
    parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)
    
    initproportion <- unlist(read.table(file="initdata",header=T)[1,])
    init <- initproportion * parms["PopSize"]
    init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)
    
    Result <- cbind(expand.grid(wd.vec, wr.vec), VaccRbe, 0, 0, 0, 0, 0, 0)
    colnames(Result) <- c("wd", "wr", "vr", "maofi", "totS", "R0First_init", "R0First_final", "R0Re_init", "R0Re_final")
    ##maofi:mean age of first infections

    counter <- 0
    for(j.piad in 1:length(wr.vec)) {
        for(i.piad in 1:length(wd.vec)) {
            counter <- counter + 1
            wr <- wr.vec[j.piad]; wd <- wd.vec[i.piad]
            WnRt = wr
            SucRt1ratio = wd
            RelContRt1 = SucRt1ratio
            Rec1tRratio = SucRt1ratio
            CntDurFrct = RelContRt1 * Rec1tRratio
            gO1 = gW1*RecOtWratio
            gWR = gW1/Rec1tRratio
            gOR = gW1/Rec1tRratio*RecOtWratio
            parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)
            out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod", events=list(func="eradication", time=VT+VRampT))
            source("derivedVariables.R", local=TRUE)
            lambdaestimate <- -log(out[500,83] / sum(out[500,43:83]))/5 #the age distribution is exponential with rate lambda
            Result[counter,4] <- sum(1:40 * out[500,43:82]) / sum(out[500,43:83]) * (5/40) + (5+1/lambdaestimate)*exp(-5*lambdaestimate)
            # fourth column is the mean age of first infections
            Result[counter,5] <- totS[500]
            Result[counter,6] <- ReffFrst[500]
            Result[counter,7] <- ReffFrst[1500]
            Result[counter,8] <- ReffRe[500]
            Result[counter,9] <- ReffRe[1500]
        }
    }
    Result
}

save(parms, Result, file=paste("meanageFI_R0_150223.RData",sep=""))



