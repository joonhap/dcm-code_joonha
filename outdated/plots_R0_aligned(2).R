## unlike plots_R0_aligned.R, in this code the vaccination level was not set to equal the minimum possible value to achieve stable eradication. The vaccination level was kept the same for all five plots.

wr.vec <- c(0.30, 0.15, 0.08, 0.06, 0.03, 0.02)
wd.vec <- c(0.32, 0.36, 0.42, 0.46, 0.60, 0.72)

con = 390

library(deSolve)
#system("R CMD SHLIB odemod.c")
dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))
n = 40
StartT = -40
maxT = 110
times <- seq(from=StartT, to=maxT, by=.1)

m = 0.02
VT = 10
VaccRbe = 0.6
VRampT = 1
PopSize = 1e6
ErLv = 1
InfPerAFP = 100
FolYrs = 15
gW1 = 13
RecOtWratio = 2
con = 390
tp = 0.5
RelContOtW = 0.5
AFPLv = InfPerAFP
WnRt = 0.1
SucRt1ratio = 0.6
RelContRt1 = SucRt1ratio
Rec1tRratio = SucRt1ratio
CntDurFrct = RelContRt1 * Rec1tRratio
gO1 = gW1*RecOtWratio
gWR = gW1/Rec1tRratio
gOR = gW1/Rec1tRratio*RecOtWratio
parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)

initproportion <- unlist(read.table(file="initdata",header=T)[1,])
init <- initproportion * parms["PopSize"]
init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)

pos <- (round(-StartT/(times[2]-times[1]))+1):length(times) #indices for positive time points

pdf(paste('pdf/plots_R0area_aligned_con_',con,'.pdf',sep=''), width=15, height=4.5)
par(mfrow=c(1,length(wr.vec)), mar=c(0,1,0,0), oma=c(4,4,3,1))
for (i.ptd in 1:length(wr.vec)) {
    WnRt <- wr <- wr.vec[i.ptd]
    SucRt1ratio <- wd <- wd.vec[i.ptd]
    RelContRt1 = SucRt1ratio
    Rec1tRratio = SucRt1ratio
    CntDurFrct = RelContRt1 * Rec1tRratio
    gO1 = gW1*RecOtWratio
    gWR = gW1/Rec1tRratio
    gOR = gW1/Rec1tRratio*RecOtWratio
    parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)
    
    out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
    source("derivedVariables.R")
    plot(times[pos], ReffTot[pos], type='l',ylim=c(0,1.4), yaxt='n', xlab='', ylab='')
    if (i.ptd==1) axis(side=2)
    mtext(side=3, line=-1.5, text=bquote(nu*" = "*.(wr)*" /yr"))
    mtext(side=3, line=-2.7, text=bquote(kappa["susc"]*" = "*kappa["cont"]*" = "*kappa["dur"]*" = "*.(wd)))
    points(times[pos], ReffRe[pos], type='l', col='blue')
    points(times[pos], ReffFrst[pos], type='l', col='green')
    ex1 <- min(1501, min(which(ReffTot[510:1501] > 1))+500)
    if (ex1 < 1501) {
        polygon(times[c(500:ex1,ex1)], c(ReffTot[500:ex1],1), col='yellow')
        area = sum((1-ReffTot[500:ex1])*(times[2]-times[1]))
        mtext(side=3, line=-3.9, text=paste("area = ",floor(100*area)/100,sep=""), cex=1)
    }
    abline(h=1, lty=3)
    abline(v=VT, col='orange')
}
mtext(side=1, line=2.5, outer=TRUE, text="Time (years)")
mtext(side=2, line=2, outer=TRUE, text=expression(R[0]))
mtext(side=3, line=0.5, outer=TRUE, text=bquote(V["final"]*" = "*.(VaccRbe)*" /yr,  "*tau["ramp"]*" = "*.(VRampT)*" yr"), cex=1.2)
dev.off()
