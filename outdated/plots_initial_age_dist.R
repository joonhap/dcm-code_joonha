library(deSolve)
system("R CMD SHLIB odemod.c")

n = 40
StartT = 0
maxT = 100
times <- seq(from=StartT, to=maxT, by=.1)

m = 0.02
VT = 100
VaccRbe = 0.5
VRampT = 5
PopSize = 1e6
ErLv = 1
InfPerAFP = 100
FolYrs = 15
gW1 = 13
RecOtWratio = 2
con = 390
tp = 0.5
RelContOtW = 0.5
AFPLv = InfPerAFP
WnRt = 0.1
SucRt1ratio = 0.6
RelContRt1 = SucRt1ratio
Rec1tRratio = SucRt1ratio
CntDurFrct = RelContRt1 * Rec1tRratio
gO1 = gW1*RecOtWratio
gWR = gW1/Rec1tRratio
gOR = gW1/Rec1tRratio*RecOtWratio
parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)

initproportion <- unlist(read.table(file="initdata",header=T)[1,])
init <- initproportion * parms["PopSize"]
init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)


### start simulations in parallel
CLUSTER=TRUE
CORES=99

require(doParallel)
if(CLUSTER){
    cl <- makeCluster(CORES+1)
    registerDoParallel(cl)
} else {
    registerDoParallel(cores=CORES+1)
}

wr.v <- seq(0.01, 0.4, by=0.01)
wd.v <- seq(0.1, 0.9, by=0.02)
wpars <- expand.grid(wr.v, wd.v)
colnames(wpars) <- c("wr","wd")

###pdf(paste("pdf/plot_FI_initial_age_dist_con_",con,"_150215.pdf",sep=""))
##par(mfcol=c(5,5), mar=c(1.5,1,1.5,1), oma=c(4.5,4,4,1))

Result <- foreach(i.stb=1:(length(wr.v)*length(wd.v)), .packages='deSolve', .combine='rbind', .export=c('n', 'StartT', 'maxT', 'times', 'm', 'VT', 'VaccRbe', 'VRampT', 'PopSize', 'ErLv', 'InfPerAFP', 'FolYrs', 'gW1', 'RecOtWratio', 'con', 'tp', 'RelContOtW', 'AFPLv', 'initproportion', 'init', 'wr.v', 'wd.v', 'wpars')) %dopar% {
    
    wr = wpars[i.stb,1]
    wd = wpars[i.stb,2]

    dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))

    WnRt = wr
    SucRt1ratio = wd
    RelContRt1 = SucRt1ratio
    Rec1tRratio = SucRt1ratio
    CntDurFrct = RelContRt1 * Rec1tRratio
    gO1 = gW1*RecOtWratio
    gWR = gW1/Rec1tRratio
    gOR = gW1/Rec1tRratio*RecOtWratio
    parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)

    out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
    source("derivedVariables.R", local=TRUE)
    ##barplot(out[1000,43:83], xaxt='n', ylim=c(0,400))
    lambdaestimate <- -log(out[1000,83] / sum(out[1000,43:83]))/5 #the age distribution is exponential with rate lambda
    c(sum(1:40 * out[1000,43:82]) / sum(out[1000,43:83]) * (5/40) + (5+1/lambdaestimate)*exp(-5*lambdaestimate), totS[1000])
    ##mtext(side=3, line=1.5, text=paste("totW=",floor(totWPV1[1000]/100)/10,"K,  ","totP=",floor(totR2[1000]/1000),"K",sep=""), cex=.75)
    ##mtext(side=3, line=0, text=bquote(paste(r^{First},"=", .(floor(ReffFrst[1000]*100)/100), ",  ", r^{Re},"=", .(floor(ReffRe[1000]*100)/100))), cex=.75)
    ##if(j.piad == 1)
    ##    mtext(side=2, line=2, text=wd.vec[i.piad])
    ##if(i.piad == 5)
    ##    mtext(side=1, line=1, text=wr.vec[j.piad])
}

Result <- cbind(wpars, Result)
colnames(Result) <- c("wr", "wd", "maofi", "totS")
###maofi:mean age of first infections

##mtext(side=3, line=2, outer=TRUE, text=paste(con," contacts per year,  R0 = ",floor(con*0.5/gW1*10)/10,sep=""))
##mtext(side=1, line=2, outer=TRUE, text="Waning rate (per year)")
##mtext(side=2, line=2.5, outer=TRUE, text="Relative susceptability, contagiousness, and duration of reinfections")

###dev.off()
if(CLUSTER) stopCluster(cl)

save(parms,Result,file=paste("RData/meanageFI_totS_con_",con,"_150318.RData",sep=""))
