library(deSolve)
system("R CMD SHLIB odemod.c")
dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))

n = 40
StartT = -50
maxT = 100
times <- seq(from=StartT, to=maxT, by=.1)
m = 0.02
VT = 0
VaccRbe = 0.35
VRampT = 15
PopSize = 1e6
ErLv = 1
InfPerAFP = 100
FolYrs = 15
gW1 = 13
RecOtWratio = 2
con = 390
tp = 0.5
RelContOtW = 0.5
AFPLv = InfPerAFP
WnRt = 0.03
SucRt1ratio = 0.6
RelContRt1 = SucRt1ratio
Rec1tRratio = SucRt1ratio
CntDurFrct = RelContRt1 * Rec1tRratio
gO1 = gW1*RecOtWratio
gWR = gW1/Rec1tRratio
gOR = gW1/Rec1tRratio*RecOtWratio
parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)


initproportion <- unlist(read.table(file="initdata",header=T)[1,])
init <- initproportion * parms["PopSize"]
init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)

pos <- (-StartT/(times[2]-times[1])+1):length(times) #indices for positive time points

par(mfrow=c(3,1), mar=c(2,2,2,2), oma=c(1,3,1,1))

# tot WPV plot
parms["VaccRbe"] <- VaccRbe <- 1.2
out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
source("derivedVariables.R")
plot(times[pos], totWPV[pos], type='l', xlab=NA, ylab=NA, ylim=c(0, 2.6e3), cex.axis=1.3)#,xlim=c(140,200))
mtext("(b) Vaccination rate: 1.2 / yr", side=3, line= 0.5, cex=1.3)
abline(v=FrstAFPTm, col='blue', lwd=1.5) # draw a vertical line at FrstAFPTm
abline(v=ScndAFPTm, col='red', lwd=1.5) # draw a vertical line at ScndAFPTm
legend('topright', legend=c('Last AFP before silence', 'Resurgent AFP'), col=c('blue','red'), lty=1, lwd=1.5, cex=1.5, bg='white') # add a legend to the plot

# LmbdaW, LmbdaO plot
plot(times[pos], LmbdaW[pos], type='l', main='LmbdaW(black), LmbdaO(blue)')
points(times[pos], LmbdaO[pos], type='l', col='blue')

# Re plot
plot(times[pos], ReffTot[pos], type='l', main='Reff, Reff(Re)--blue, Reff(First)--green', ylim=c(0,1.5))
points(times[pos], ReffRe[pos], type='l', col='blue')
points(times[pos], ReffFrst[pos], type='l', col='green')
