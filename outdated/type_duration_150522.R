### Eradication status and silent circulation duration ###
###    under varying vaccine rate and ramp up time     ###

## code chunk for automated PBS scripts
args = commandArgs(trailingOnly = T)
R0 = as.numeric(args[1])
AverageAgeAtI1 = as.numeric(args[2])
InfPerAFP = as.numeric(args[3])#100 or 200
OvsW = as.numeric(args[4])#.25 or .375

system("R CMD SHLIB odemod.c")

n = 40
StartT = -150
maxT = 100
times <- seq(from=StartT, to=maxT, by=.1)

# define a parameter vector to be passed to "ode" function
m = 0.02
VT = 0
VaccRbe = 0.9 # to be varied
VRampT = 5 # to be varied
PopSize = 1e6
ErLv = 1
InfPerAFP = InfPerAFP
FolYrs = 15
gW1 = 13
tp = 0.5
con = R0 * gW1 / tp
RecOtWratio = 1/sqrt(OvsW)
RelContOtW = sqrt(OvsW)
AFPLv = InfPerAFP
WnRt = 0.30 # to be varied
SucRt1ratio = 0.32 # to be varied
RelContRt1 = SucRt1ratio
Rec1tRratio = SucRt1ratio
CntDurFrct = RelContRt1 * Rec1tRratio
gO1 = gW1*RecOtWratio
gWR = gW1/Rec1tRratio
gOR = gW1/Rec1tRratio*RecOtWratio
parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)

# set up an inital condition
initproportion <- unlist(read.table(file="initdata",header=T)[1,])
init <- initproportion * parms["PopSize"]
init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)

# define parameter sweep over (VRampT, VaccRbe)
VRampTgrid <- seq(from= .4, to= 20, by=0.4)
VaccRbegrid <- seq(from= 0.1, to= 2.5, by=0.02)

load(paste('RData/wr_wd_maofi_',AverageAgeAtI1,'_con_390','.RData',sep=''))
wd.v <- wd.vec
wr.v <- wr.vec

## register parallel processors
CLUSTER=TRUE
CORES=63

require(doParallel)
if(CLUSTER){
 cl <- makeCluster(CORES+1)
 registerDoParallel(cl)
} else {
 registerDoParallel(cores=CORES+1)
}

###
for ( index in 1:length(wr.v) ) {
###

    wr = wr.v[index]
    wd = wd.v[index]
    
    ## SOLVE DIFFERENTIAL EQUATION ##
    ## parameter sweep over (VRampT, VaccRbe)
    CombinedResult <- foreach(i.td=1:length(VRampTgrid), .packages='deSolve', .combine=rbind, .export=c('n', 'StartT', 'maxT', 'times', 'm', 'VT', 'PopSize', 'ErLv', 'InfPerAFP', 'FolYrs', 'gW1', 'RecOtWratio', 'con', 'tp', 'RelContOtW', 'AFPLv', 'initproportion', 'init', 'wr', 'wd', 'VaccRbegrid', 'VRampTgrid')) %dopar% {
        dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))
        
        VRampT = VRampTgrid[i.td]
        WnRt = wr
        SucRt1ratio = wd
        RelContRt1 = SucRt1ratio
        Rec1tRratio = SucRt1ratio
        CntDurFrct = RelContRt1 * Rec1tRratio
        gO1 = gW1*RecOtWratio
        gWR = gW1/Rec1tRratio
        gOR = gW1/Rec1tRratio*RecOtWratio
        parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)
        
        t(sapply(VaccRbegrid, function(VR) {
            parms['VaccRbe'] = VaccRbe = VR
            out <- ode(init, times, func = "derivs", parms = parms, dllname = "odemod", initfun= "initmod")
            source('derivedVariables.R', local=TRUE)
            output <- c(VaccRbe, VRampT, FrstAFPTm, ScndAFPTm, Erad3Tm, Silent3Type, Silent3Dur)
            ## check if there occurs stable eradication
            if(max(ReffTot[((VT-StartT)/(times[2]-times[1])+20):length(times)]) > 1.0-5e-4) {
                output <- c(output, 0) # not stable eradication
            } else {
                output <- c(output, 1) # stable eradication
            }
            output
        } ))
    }
    
    colnames(CombinedResult) <- c("VaccRbe", "VRampT", "FrstAFPTm", "ScndAFPTm", "Erad3Tm", "Silent3Type", "Silent3Dur", "StableErad")
    
    save(parms, CombinedResult, file=paste("RData/type_duration_con_", con, "_wr_", wr, "_wd_", wd,"_AvAgeFI_",AverageAgeAtI1,"_InfPerAFP_",InfPerAFP,"_OvsW_",OvsW, "_150522.RData",sep=""))
    
}


stopCluster(cl)
