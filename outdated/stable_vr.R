### Vaccination rate needed for stable eradication ###
library(deSolve)
system("R CMD SHLIB odemod.c")

n = 40
StartT = -50
maxT = 100
times <- seq(from=StartT, to=maxT, by=.1)

# define parms the same as in the foreach loop, to be saved.
m = 0.02
VT = 00
VaccRbe = 0.9
VRampT = 1
PopSize = 1e6
ErLv = 1
InfPerAFP = 100
FolYrs = 15
gW1 = 13
RecOtWratio = 2
con = 390
tp = 0.5
RelContOtW = 0.5
AFPLv = InfPerAFP
WnRt = 0.038
SucRt1ratio = 0.63
RelContRt1 = SucRt1ratio
Rec1tRratio = SucRt1ratio
CntDurFrct = RelContRt1 * Rec1tRratio
gO1 = gW1*RecOtWratio
gWR = gW1/Rec1tRratio
gOR = gW1/Rec1tRratio*RecOtWratio
parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)

initproportion <- unlist(read.table(file="initdata",header=T)[1,])
init <- initproportion * parms["PopSize"]
init <- c(init, CumFrstWInf=0, CumFrstFrFrst=0, CumFrstFrRe=0, CumReFrFrst=0, CumReFrRe=0, EradStatus=0)

### load the find_stable_vr.R function onto workspace
source("find_stable_vr.R")


### start simulations in parallel
CLUSTER=TRUE
CORES=99

require(doParallel)
if(CLUSTER){
 cl <- makeCluster(CORES+1)
 registerDoParallel(cl)
} else {
 registerDoParallel(cores=CORES+1)
}


wr.v <- seq(0.01, 0.4, by=0.01)
wd.v <- seq(0.1, 0.9, by=0.02)
wpars <- expand.grid(wr.v, wd.v)
colnames(wpars) <- c("wr","wd")

thresh_vr <- foreach(i.stb=1:(length(wr.v)*length(wd.v)), .packages='deSolve', .combine=c, .export=c('n', 'StartT', 'maxT', 'times', 'm', 'VT', 'PopSize', 'ErLv', 'InfPerAFP', 'FolYrs', 'gW1', 'RecOtWratio', 'con', 'tp', 'RelContOtW', 'AFPLv', 'initproportion', 'init', 'wr.v', 'wd.v', 'wpars')) %dopar% {

    wr = wpars[i.stb,1]
    wd = wpars[i.stb,2]

    dyn.load(paste("odemod", .Platform$dynlib.ext, sep = ""))

    WnRt = wr
    SucRt1ratio = wd
    RelContRt1 = SucRt1ratio
    Rec1tRratio = SucRt1ratio
    CntDurFrct = RelContRt1 * Rec1tRratio
    gO1 = gW1*RecOtWratio
    gWR = gW1/Rec1tRratio
    gOR = gW1/Rec1tRratio*RecOtWratio
    parms <- c(m = m, VT = VT, VaccRbe = VaccRbe, VRampT = VRampT, PopSize = PopSize, ErLv = ErLv, InfPerAFP = InfPerAFP, FolYrs = FolYrs, gW1 = gW1, CntDurFrct = CntDurFrct, SucRt1ratio = SucRt1ratio, RecOtWratio = RecOtWratio, con = con, tp = tp, WnRt = WnRt, RelContOtW = RelContOtW, RelContRt1 = RelContRt1, Rec1tRratio = Rec1tRratio, gO1 = gO1, gWR = gWR, gOR = gOR, AFPLv = AFPLv)

    find_stable_vr(parms)
}

thresh_vr <- cbind(wpars, thresh_vr=thresh_vr)

save(parms, thresh_vr, file=paste("RData/stable_vr_con_",con,"_150318.RData", sep=""))
