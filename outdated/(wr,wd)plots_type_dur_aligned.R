library(ggplot2)
library(plyr) # for subset plotting
library(grid)

con <- 390
vr.v = c(0.5,1,1.5,2,2.5)
vrampt.v = c(15,12,9,6,3)

wdgrid <- seq(0.02, 0.90, by=0.02) # waning depth
wrgrid <- seq(0.01, 0.40, by=0.01) # waning rate 
nwd <- length(wdgrid)
nwr <- length(wrgrid)

plots <- list()

counter <- 0
for(h.ptd in 1:length(vrampt.v)) {
    for(i.ptd in 1:length(vr.v)) {
        counter <- counter+1
        vr <- vr.v[i.ptd]
        vrampt <- vrampt.v[h.ptd]
        
        load(paste("RData/(wr,wd)typ_dur_con_", con, "_vr_", vr, "_vrampt_", vrampt, "_150313.RData", sep=""))
        
        Result <- as.data.frame(Result)
        ## if neither second AFP or eradication happened until the end (rare), say that it belongs 'case comeback' category
        Result[,"Silent3Type"] <- ifelse(Result[,"Silent3Type"]!=3, Result[,"Silent3Type"], 2)
        Result[,"Silent4Type"] <- ifelse(Result[,"Silent4Type"]!=3, Result[,"Silent4Type"], 2)
        ## if stable eradication is achieved, put it in a separate category
        Result[,"Silent3Type"] <- ifelse(Result[,"StableErad"]==1, 4, Result[,"Silent3Type"])
        Result[,"Silent4Type"] <- ifelse(Result[,"StableErad"]==1, 4, Result[,"Silent4Type"])
        
        ## mark VaccRbe values at which either Silent duration either starts or ceases to exceed 3 yrs.
        Silent3yrStartEnd <- data.frame(NULL)
        for(j.ptd in 1:nwd) {
            Silent3yrStart <- 0; Silent3yrEnd <- 0
            for(k.ptd in 1:nwr) {
                if(Silent3yrStart == 0 & Result[(j.ptd-1)*nwr + k.ptd,"Silent3Dur"] > 3)
                    Silent3yrStart <- k.ptd
                if(Silent3yrStart != 0 & k.ptd > Silent3yrStart & Result[(j.ptd-1)*nwr + k.ptd,"Silent3Dur"] > 3)
                    Silent3yrEnd <- k.ptd
            }
            if (Silent3yrStart != 0)
                Silent3yrStartEnd <- rbind(Silent3yrStartEnd, c(wrgrid[Silent3yrStart], wdgrid[j.ptd]))
            if (Silent3yrEnd != 0)
                Silent3yrStartEnd <- rbind(Silent3yrStartEnd, c(wrgrid[Silent3yrEnd], wdgrid[j.ptd]))
        }
        colnames(Silent3yrStartEnd) <- c("wr","wd")
        
        ## Eradication status / Silent circulation duration plot ###
        ## the color is determined by the 'Silent3Type' column, and the intensity by the 'Silent3Dur'.
        p <- ggplot(data=Result, asp=1)
        p <- p + geom_tile(aes(x=wr, y=wd, fill=factor(Silent3Type), alpha=pmin(8,pmax(0,Silent3Dur))))
        p <- p + scale_fill_manual(values = c('0'='white','1'='yellow','2'='red', '4'='green'), breaks=0:4, guide=FALSE)
        p <- p + scale_alpha(name="Silent duration",limits=c(0,8), guide=FALSE)
        p <- p + theme(axis.text.x=element_blank(), axis.text.y=element_blank(), plot.margin=unit(c(0,0,0,0),"pt"), axis.ticks=element_blank(), panel.background=element_rect(fill="white"))
        p <- p + labs(y="") + labs(x="")
        p <- p + geom_point(data=Silent3yrStartEnd, aes(x=wr, y=wd), size=0.9, alpha=1)
        
        plots[[counter]] <- p
    }
}
for (i2.ptd in 0:4)
    plots[[5*i2.ptd+1]] <- plots[[5*i2.ptd+1]] + labs(y=vrampt.v[i2.ptd+1])
for (i2.ptd in 1:5)
    plots[[4*5+i2.ptd]] <- plots[[4*5+i2.ptd]] + labs(x=vr.v[i2.ptd]) 

### scales ###
db <- matrix(0,nrow=5,ncol=4)
colnames(db) <- c("wr", "wd", "Silent3Type", "Silent3Dur")
db[,1] <- 1:5; db[,2] <- 1:5; db[,3] <- c(0,1,2,2,4); db[,4] <- 4*1:5
db <- as.data.frame(db)
plotforlegend <- ggplot(db, aes(x=wr, y=wd, fill=factor(Silent3Type), alpha=pmin(8,pmax(0,Silent3Dur))), asp=1) +
    geom_tile() + 
    scale_fill_manual(name="Classification",values = c('0'='white','2'='red','1'='yellow','4'='green'),breaks=c('0','2','1','4'),labels=c('no eradication','case comeback','unstable eradication','stable eradication')) +
    scale_alpha(name="Silent duration (years)",limits=c(0,8)) +
    theme(axis.text=element_blank(), axis.title=element_blank(), panel.grid.minor=element_blank(), panel.grid.major=element_blank(), axis.ticks=element_blank(), panel.border=element_blank(), panel.background=element_rect(fill="white"),  legend.text= element_text(size=10), legend.title= element_text(size=10))
require(gridExtra)
g <- ggplotGrob(plotforlegend + theme(legend.position="right", legend.key.size=unit(.2,"in"), legend.margin=unit(.2,"in")))$grobs
legend <- g[[which(sapply(g, function(x) x$name) == "guide-box")]]
lwidth <- sum(legend$width)
dev.off()

axisp <- ggplot(data.frame(x=1,y=1),aes(x=x,y=y)) + xlim(c(0,0.4)) + ylim(c(0,0.9)) + geom_point(color="white") + theme(axis.title=element_text(size=10), panel.border=element_blank(), panel.background=element_rect(fill="white"), plot.margin=unit(c(40,40,0,0),"pt"), aspect.ratio=1) + labs(x=expression("Waning rate (yr"^{-1}*")"), y="Waning depth")

pdf(paste("pdf/(wr,wd)plots_type_dur_con_",con,"_150315.pdf", sep=""), width=13, height=11)
grid.arrange(
    do.call(arrangeGrob, args=c(lapply(plots, function(x) x + theme(legend.position="none")), ncol=5)),
    arrangeGrob(axisp, legend, ncol=1), 
    ncol = 2,
    widths = unit.c(unit(10,"in"), unit(2,"in")),
    heights = unit(10, "in"),
    sub = textGrob("Vaccination rate (per year)", vjust=-0.5, gp=gpar(fontsize=15)),
    left = textGrob("Vaccination ramp up time (years)", rot = 90, vjust=1, gp=gpar(fontsize=15)),
    main = textGrob(paste(con," contacts per year,  R0(pure susceptibles) = ",floor(con*0.5/13*10)/10,sep=""), vjust=1, gp=gpar(fontsize=15))
   ) 
dev.off()

