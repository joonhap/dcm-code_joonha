require(ggplot2)
require(grid)
require(plyr) #for plotting subsets in ggplot

## generate aligned plots
datafiles <- system("ls RData/*_150522.RData", intern=TRUE)

for(datafilename in datafiles) {

underbars <- gregexpr(pattern='_', datafilename)[[1]]

AvAgeFI = as.numeric(substr(datafilename, underbars[9]+1, underbars[10]-1))
con = as.numeric(substr(datafilename, underbars[3]+1, underbars[4]-1))

wr = as.numeric(substr(datafilename, underbars[5]+1, underbars[6]-1))
wd = as.numeric(substr(datafilename, underbars[7]+1, underbars[8]-1))
InfPerAFP = as.numeric(substr(datafilename, underbars[11]+1, underbars[12]-1))
OvsW = as.numeric(substr(datafilename, underbars[13]+1, underbars[14]-1))

VRampTgrid <- seq(from= 0.4, to= 20, by=0.4); nvrt <- length(VRampTgrid)
VaccRbegrid <- seq(from= 0.1, to= 2.5, by=0.02); nvrb <- length(VaccRbegrid)

load(datafilename)

## mark a region where eradication happens earlier than the end of vaccination ramp-up
EarlyErad <- as.data.frame(CombinedResult[(CombinedResult[,"Erad3Tm"]<CombinedResult[,"VRampT"]), c("VaccRbe","VRampT")])

## stable eradication is marked in green
StableErad <- sapply(1:dim(CombinedResult)[1], function(x) all(CombinedResult[x:(((x-1)%/%nvrb+1)*nvrb),"StableErad"]==1)) ## since the simulation stops at finite time, in some cases (though very rare) unstable eradication can be marked as stable. So we need to check that all higher vaccination rates result in stable eradication.
CombinedResult[,"Silent3Type"] <- ifelse(StableErad==1 & CombinedResult[,"Silent3Type"]==1, 4, CombinedResult[,"Silent3Type"])
## we no longer distinguish the points where no second AFP nor eradication happens until simulation end time.
## instead of purple (as done previously), mark it in red
CombinedResult[,"Silent3Type"] <- ifelse(CombinedResult[,"Silent3Type"]==3, 2, CombinedResult[,"Silent3Type"])        

CombinedResult <- as.data.frame(CombinedResult)
## Eradication status / Silent circulation duration plot ###
## plot of eradication status and the duration of silent circulation
## the color is determined by the 'Silent3Type' column, and the intensity by the 'Silent3Dur'.


VaccRInc <- round(CombinedResult[,"VaccRbe"] / CombinedResult[,"VRampT"], digits=2)
VaccRatLP <- round(ifelse(CombinedResult[,"Silent3Type"]==0 | CombinedResult[,"VRampT"]<CombinedResult[,"FrstAFPTm"], CombinedResult[,"VaccRbe"], CombinedResult[,"VaccRbe"]/CombinedResult[,"VRampT"]*CombinedResult[,"FrstAFPTm"]) / 0.02) * 0.02
CombinedResult <- cbind(CombinedResult, VaccRInc, VaccRatLP)
rm(VaccRatLP)
CombinedResult[,"FrstAFPTm"] <- ifelse(CombinedResult[,"FrstAFPTm"]==100.0, 0.0, CombinedResult[,"FrstAFPTm"])
ProlongedSC <- CombinedResult[CombinedResult[,"Silent3Dur"]>3.0,]


filename1=paste("plots/[VaccRatLP_LastPolioTime]type_duration_AvAgeFI_",AvAgeFI,"_con_", con, "_wr_", round(wr,3), "_wd_", round(wd,3), "_InfPerAFP_",InfPerAFP,"_OvsW_",OvsW, "_150921.pdf",sep="")
pdf(filename1, height=6, width=6)
p <- ggplot(data=CombinedResult, asp=1) + xlim(0,max(ProlongedSC[,"VaccRatLP"])+0.5)
p <- p + geom_tile(aes(x=VaccRatLP, y=FrstAFPTm, fill=factor(Silent3Type), alpha=pmin(8,pmax(0,Silent3Dur))))
p <- p + scale_fill_manual(values = c('0'='white','1'='blue','2'='red','4'='green'), breaks=c(0,1,2,4), guide=FALSE)
p <- p + scale_alpha(name="Silent duration",limits=c(0,8), guide=FALSE)
p <- p + theme(plot.margin=unit(c(5,10,0,0),"pt"), axis.ticks=element_blank(), panel.background=element_rect(fill="white"), axis.text=element_text(size=20), axis.title=element_text(size=22), plot.title=element_text(size=18))
p <- p + labs(y="Time to last polio case (years)") + labs(x=expression("Vaccination rate at last polio case (yr"^{-1}*")")) + ggtitle(paste('Average Age at first infection = ',AvAgeFI,',  R0 = ', con/26,',\n Waning rate = ',round(wr,3),' /yr,  Waning depth = ',round(wd^3,2),',\n IPR = ', InfPerAFP,',  Vaccine transmissibility = ', OvsW, sep=''))
p <- p + geom_point(data=ProlongedSC, aes(x= VaccRatLP, y= FrstAFPTm), shape=0, size=3, alpha=.5)
print(p)
dev.off()

filename2=paste("plots/[VaccRLevel_LastPolioTime]type_duration_AvAgeFI_",AvAgeFI,"_con_", con, "_wr_", round(wr,3), "_wd_", round(wd,3), "_InfPerAFP_",InfPerAFP,"_OvsW_",OvsW, "_150921.pdf",sep="")
pdf(filename2, height=6, width=6)
p <- ggplot(data=CombinedResult, asp=1)
p <- p + geom_tile(aes(x=VaccRbe, y=FrstAFPTm, fill=factor(Silent3Type), alpha=pmin(8,pmax(0,Silent3Dur))))
p <- p + scale_fill_manual(values = c('0'='white','1'='blue','2'='red','4'='green'), breaks=c(0,1,2,4), guide=FALSE)
p <- p + scale_alpha(name="Silent duration",limits=c(0,8), guide=FALSE)
p <- p + theme(plot.margin=unit(c(5,10,0,0),"pt"), axis.ticks=element_blank(), panel.background=element_rect(fill="white"), axis.text=element_text(size=20), axis.title=element_text(size=22), plot.title=element_text(size=18))
p <- p + labs(y="Time to last polio case (years)") + labs(x=expression("Vaccination rate when leveled off (yr"^{-1}*")")) + ggtitle(paste('Average Age at first infection = ',AvAgeFI,',  R0 = ', con/26,',\n Waning rate = ',round(wr,3),' /yr,  Waning depth = ',round(wd^3,2),',\n IPR = ', InfPerAFP,',  Vaccine transmissibility = ', OvsW, sep=''))
p <- p + geom_point(data=ProlongedSC, aes(x= VaccRbe, y= FrstAFPTm), shape=1, size=1.4, alpha=.5)
print(p)
dev.off()

}
