/* file odemod.c */
// include libraries
#include <R.h> // needed for connecting to R
#include <math.h> // needed for simple math functions, such as sqrt

// define parameters (globally available in this file)
static double parms[28];
#define n 40
#define m parms[0]
#define VT parms[1]
#define VaccRbe parms[2]
#define VRampT parms[3]
#define PopSize parms[4]
#define ErLv parms[5]
#define InfPerAFP parms[6]
#define FolYrs parms[7]
#define gW1 parms[8]
#define CntDurFrct parms[9]
#define SucRt1ratio1 parms[10]
#define RecOtWratio parms[11]
#define con parms[12]
#define tp parms[13]
#define WnRt parms[14]
#define RelContOtW parms[15]
#define RelContRt1 parms[16]
#define Rec1tRratio parms[17]
#define gO1 parms[18]
#define gWR parms[19]
#define gOR parms[20]
#define AFPLv parms[21]
#define SucRt1ratio2 parms[22]
#define SucRt1ratio3 parms[23]
#define SucRt1ratio4 parms[24]
#define WnRt1 parms[25]
#define WnRt2 parms[26]
#define WnRt3 parms[27]

/* Initializer */
// needed to import parms from R (no need to modify this function, except for `Nparms')
void initmod(void (*odeparms)(int *, double *))
{
  int Nparms = 28; // the number of parameters received
  odeparms(&Nparms, parms);
}

/* Derivatives */
// defines the model differential equations
// this function is iteratively called to compute the derivatives at give state values
void derivs (int *neq, double *t, double *y, double *ydot, double *yout, int *ip)
// arguments: neq--number of equations (internally handled), t--current time, y--the current state vector (input), ydot--the vector of derivatives (output), yout--the vector of additional return values (output), ip--number of integer parameters (for internal use)
{
  // y is actually the `pointer' to the physical memory corresponding to the first element of the input state vector. First (n+1) of them correspond to S, second (n+1) correspond to WPV1, etc. (S, WPV1, WPVR, OPV1, OPVR, R, R2). After 7*(n+1) entries, there follow CumFrstWInf, CumFrstFrFrst, CumFrstFrRe, CumReFrFrst, CumReFrRe.
  double* S = y;
  double* WPV1 = y+(n+1);
  double* WPVR1[5];
  WPVR1[0] = y+2*(n+1); WPVR1[1] = WPVR1[0]+(n+1); WPVR1[2] = WPVR1[1]+(n+1); WPVR1[3] = WPVR1[2]+(n+1); WPVR1[4] = WPVR1[3]+(n+1);
  double* WPVR2[5];
  WPVR2[0] = y+7*(n+1); WPVR2[1] = WPVR2[0]+(n+1); WPVR2[2] = WPVR2[1]+(n+1); WPVR2[3] = WPVR2[2]+(n+1); WPVR2[4] = WPVR2[3]+(n+1);
  double* OPV1 = y+12*(n+1);
  double* OPVR1[5];
  OPVR1[0] = y+13*(n+1); OPVR1[0] = OPVR1[1]+(n+1); OPVR1[2] = OPVR1[1]+(n+1); OPVR1[3] = OPVR1[2]+(n+1); OPVR1[4] = OPVR1[3]+(n+1);
  double* OPVR2[5];
  OPVR2[0] = y+18*(n+1); OPVR2[0] = OPVR2[1]+(n+1); OPVR2[2] = OPVR2[1]+(n+1); OPVR2[3] = OPVR2[2]+(n+1); OPVR2[4] = OPVR2[3]+(n+1);
  double* R1[5];
  R1[0] = y+23*(n+1); R1[1] = R1[0]+(n+1); R1[2] = R1[1]+(n+1); R1[3] = R1[2]+(n+1); R1[4] = R1[3]+(n+1);
  double* R2[5];
  R2[0] = y+28*(n+1); R2[1] = R2[0]+(n+1); R2[2] = R2[1]+(n+1); R2[3] = R2[2]+(n+1); R2[4] = R2[3]+(n+1);
  double* CumFrstWInf = y+33*(n+1);
  double* CumFrstFrFrst = y+33*(n+1)+1;
  double* CumFrstFrRe = y+33*(n+1)+2;
  double* CumReFrFrst = y+33*(n+1)+3;
  double* CumReFrRe = y+33*(n+1)+4;
  double* EradStatus = y+33*(n+1)+5;

  // define VaccRtbe as a function of t. (from 0 to VaccRbe, linear in between)
  double VaccRtbe = fmin(VaccRbe, fmax(0, VaccRbe*(t[0]-VT)/VRampT));

  double SucR1t1ratio[5];
  SucR1t1ratio[0] = ResidSusc;  // Susceptibility of R1[age,i] state divided by susceptibility of S state
  SucR1t1ratio[1] = (ResidSusc+wd1/4)^(1/3);
  SucR1t1ratio[2] = (ResidSusc+wd1*2/4)^(1/3);
  SucR1t1ratio[3] = (ResidSusc+wd1*3/4)^(1/3);
  SucR1t1ratio[4] = (ResidSusc+wd1)^(1/3);
  double SucR2t1ratio[5];
  SucR2t1ratio[0] = 0;	// Susceptibility of R2[age,i] state divided by susceptibility of S state
  SucR2t1ratio[1] = (wd2/4)^(1/3);
  SucR2t1ratio[2] = (wd2*2/4)^(1/3);
  SucR2t1ratio[3] = (wd2*3/4)^(1/3);
  SucR2t1ratio[4] = wd2^(1/3);

  double RecOtWratio = 1/RelR0OPVoWPV^0.5; // Ratio of recovery rate in OPV/WPV infections
  double RelContOtW = RelR0OPVoWPV^0.5;	// The relative contagiousness of OPV to WPV

  double RelContR1t1[5];
  for (int j=0; j<5; j++) {
    RelContR1t1[j] = SucR1t1ratio[j]; // Contagiousness of first reinfections divided by contagiousness of first infections
    RelContR2t1[j] = SucR2t1ratio[j]; // Contagiousness of subsequent reinfections divided by contagiousness of first infections
    RecR1t1ratio[j] = RelContR1t1[j]; // Ratio of recovery in second to first infections
    RecR2t1ratio[j] = RelContR2t1[j]; // Ratio of  recovery in third or higher to first infections
  }
  double gO1 = gW1*RecOtWratio; // Rate of recovery from first OPV infections
  for (int j=0; j<5; j++) {
    if (RecR1t1ratio[j]>0) {
      gWR1[j] = gW1 / RecR1t1ratio[j]; // rate of recovery from second WPV reinfections
    } else {
      gWR1[j] = 1/13;
    }
    if (RecR2t1ratio[j]>0) {
      gWR2[j] = gW1 / RecR2t1ratio[j]; // rate of recovery from third or higher WPV reinfections
    } else {
      gWR2[j] = 1/13;
    }
    gOR1[j] = gWR1[j]*RecOtWratio; // Rate of recovery from second OPV reinfections
    gOR2[j] = gWR2[j]*RecOtWratio; // Rate of recovery from third or higher OPV reinfections
  }

  double LmbdaW = 0.0;
  for (int i=0; i<n+1; i++) {
    LmbdaW += WPV1[i];
    for (int j=0; j<5; j++) {
      LmbdaW += WPVR1[j][i]*RelContR1t1[j];
      LmbdaW += WPVR2[j][i]*RelContR2t1[j];
    }
  }
  LmbdaW /= TotPop;

  double LmbdaO = 0.0;
  for (int i=0; i<n+1; i++) {
    LmbdaO += OPV1[i];
    for (int j=0; j<5; j++) {
      LmbdaO += RelContR1t1[j] * OPVR1[j][i];
      LmbdaO += RelContR2t1[j] * OPVR2[j][i];
    }
  } 
  LmbdaO *= RelContOtW / TotPop;

  // ydot is the pointer to the physical memory corresponding to the vector of derivatives (to be precise, the first element of it). Once the values in these momory slots are modified, they can be read in R directly. This is how the computed derivatives are passed to R.
  double* dS = ydot;
  double* dWPV1 = ydot+(n+1);
  double* dWPVR1[5];
  dWPVR1[0] = ydot+2*(n+1); dWPVR1[1] = dWPVR1[0]+(n+1); dWPVR1[2] = dWPVR1[1]+(n+1); dWPVR1[3] = dWPVR1[2]+(n+1); dWPVR1[4] = dWPVR1[3]+(n+1);
  double* dWPVR2[5];
  dWPVR2[0] = ydot+7*(n+1); dWPVR2[1] = dWPVR2[0]+(n+1); dWPVR2[2] = dWPVR2[1]+(n+1); dWPVR2[3] = dWPVR2[2]+(n+1); dWPVR2[4] = dWPVR2[3]+(n+1);
  double* dOPV1 = ydot+12*(n+1);
  double* dOPVR1[5];
  dOPVR1[0] = ydot+13*(n+1); dOPVR1[0] = dOPVR1[1]+(n+1); dOPVR1[2] = dOPVR1[1]+(n+1); dOPVR1[3] = dOPVR1[2]+(n+1); dOPVR1[4] = dOPVR1[3]+(n+1);
  double* dOPVR2[5];
  dOPVR2[0] = ydot+18*(n+1); dOPVR2[0] = dOPVR2[1]+(n+1); dOPVR2[2] = dOPVR2[1]+(n+1); dOPVR2[3] = dOPVR2[2]+(n+1); dOPVR2[4] = dOPVR2[3]+(n+1);
  double* dR1[5];
  dR1[0] = ydot+23*(n+1); dR1[1] = dR1[0]+(n+1); dR1[2] = dR1[1]+(n+1); dR1[3] = dR1[2]+(n+1); dR1[4] = dR1[3]+(n+1);
  double* dR2[5];
  dR2[0] = ydot+28*(n+1); dR2[1] = dR2[0]+(n+1); dR2[2] = dR2[1]+(n+1); dR2[3] = dR2[2]+(n+1); dR2[4] = dR2[3]+(n+1);
  double* dCumFrstWInf = ydot+33*(n+1);
  double* dCumFrstFrFrst = ydot+33*(n+1)+1;
  double* dCumFrstFrRe = ydot+33*(n+1)+2;
  double* dCumReFrFrst = ydot+33*(n+1)+3;
  double* dCumReFrRe = ydot+33*(n+1)+4;
  double* dEradStatus = ydot+33*(n+1)+5;

  // Derivatives
  dS[0] = m*PopSize - S[0]*c*tp*(LmbdaW +LmbdaO) - S[0]*(n/5.0) - S[0]*m - S[0]*VaccRtbe;
  for (int i=1; i<n; i++) {
    dS[i] = S[i-1]*(n/5.0) - S[i]*c*tp*(LmbdaW +LmbdaO) - S[i]*m - S[i]*VaccRtbe - S[i]* (n/5.0);
  }
  dS[n] =  S[n-1]*(n/5.0) - S[n]*c*tp*(LmbdaW +LmbdaO) - S[n]*m;
  dWPV1[0] = S[0]*c*tp*LmbdaW - WPV1[0]*gW1 - WPV1[0]*(n/5.0) - WPV1[0]*m;
  for (int i=1; i<n; i++) {
    dWPV1[i] = WPV1[i-1]*(n/5.0) + S[i]*c*tp*LmbdaW - WPV1[i]*gW1 - WPV1[i]*(n/5.0) - WPV1[i]*m;
  }
  dWPV1[n] = WPV1[n-1]*(n/5.0) + S[n]*c*tp*LmbdaW - WPV1[n]*gW1 - WPV1[n]*m;
  dOPV1[0] = S[0]*VaccRtbe + S[0]*c*tp*LmbdaO - OPV1[0]*gO1 - OPV1[0]*(n/5.0) - OPV1[0]*m;
  for (int i=1; i<n; i++) {
    dOPV1[i] = OPV1[i-1]*(n/5.0) + S[i]*VaccRtbe + S[i]*c*tp*LmbdaO - OPV1[i]*gO1 - OPV1[i]*(n/5.0) - OPV1[i]*m;
  }
  dOPV1[n] = OPV1[n-1]*(n/5.0) + S[n]*VaccRtbe + S[n]*c*tp*LmbdaO - OPV1[n]*gO1 - OPV1[n]*m;
  dR1[0][0] = WPV1[0]*gW1 + OPV1[0]*gO1 - R1[0][0]*WnRt - R1[
d/dt (R1[1..n+1,1]) = (if i > 1 then R1[i-1,1]*n/5 else 0 ) 		{aging into this group from lower}
	+ WPV1[i]*gW1 					{new recoveries from first WPV infections}
	+ OPV1[i]*gO1 						{new recoveries from first OPV infections}
	- R1[i,1]*WnRt 						{waning out of first level of R1 waning queue}
	- R1[i,1]*c*SucR1t1ratio[1]*tp*(LmbdaW + LmbdaO) 	{New O and W transmissions out of the waning queue after first infections}
	- R1[i,1] * (if i < n+1 then n/5 else 0) 			{aging out of the group}
	- R1[i,1] * (if i<n+1 then VaccRtbe*SucR1t1ratio[j] else 0)	{vaccination of the <5 age groups}
	- R1[i,1] * m						{deaths}
d/dt (R1[1..n+1,2..5]) = (if i > 1 then R1[i-1,j]*n/5 else 0 ) 		{aging into this group from lower}
	+ R1[i,j-1]*WnRt 					{waning into this waning group from from lower level wanng}
	- R1[i,j]*c*SucR1t1ratio[j]*tp*(LmbdaW + LmbdaO) 	{New O and W transmissions out of the waning queue after first infections}
	- R1[i,j]*(if i < n+1 then (n/5) else 0) 			{aging out of the group}
	- R1[i,j]* (if i<n+1 then VaccRtbe*SucR1t1ratio[j] else 0)	{vaccination of the <5 age groups}
	- R1[i,j]* (if j<5 then WnRt else 0)			{waning out of this level of R1 waning queue}
	- R1[i,j]*m						{deaths}

d/dt (R2[1..n+1,1]) = (if i > 1 then R2[i-1,j]*n/5 else 0 ) 		{aging into this group from lower}
	+ (WPVR2[i,1]*gWR2[1]+WPVR2[i,2]*gWR2[2]+WPVR2[i,3]*gWR2[3]+WPVR2[i,4]*gWR2[4]+WPVR2[i,5]*gWR2[5]) 	{Recoveries out of third or subsequent WPV infections}
	+ (WPVR1[i,1]*gWR1[1]+WPVR1[i,2]*gWR1[2]+WPVR1[i,3]*gWR1[3]+WPVR1[i,4]*gWR1[4]+WPVR1[i,5]*gWR1[5]) 	{Recoveries out of second WPV infections}
	+ (OPVR1[i,1]*gOR1[1]+OPVR1[i,2]*gOR1[2]+OPVR1[i,3]*gOR1[3]+OPVR1[i,4]*gOR1[4]+OPVR1[i,5]*gOR1[5]) 		{Recoveries out of second OPV infections}
	+ (OPVR2[i,1]*gOR2[1]+OPVR2[i,2]*gOR2[2]+OPVR2[i,3]*gOR2[3]+OPVR2[i,4]*gOR2[4]+OPVR2[i,5]*gOR2[5])  		{Recoveries out of third or subsequent WPV infections}
	- R2[i,1]*c*SucR2t1ratio[j]*tp*(LmbdaW + LmbdaO) 
	- R2[i,1]*WnRt 
	- R2[i,1]*(if i < n+1 then n/5 else 0) 
	- R2[i,1]*(if i < n+1 then VaccRtbe*SucR2t1ratio[1] else 0) 
	- R2[i,1]*m

d/dt (R2[1..n+1,2..5]) = (if i > 1 then R2[i-1,j]*n/5 else 0 ) 
	+ R2[i,j-1]*WnRt 
	- R2[i,j]*c*SucR2t1ratio[j]*tp*(LmbdaW + LmbdaO) 
	- R2[i,j]*(if i < n+1 then (n/5) else 0)
	- R2[i,j]*(if i < n+1 then VaccRtbe*SucR2t1ratio[j] else 0)
	- R2[i,j]*(if j<5 then WnRt else 0)
	- R2[i,j]*m

d/dt (WPVR1[1..n+1,1..5]) = (if i > 1 then WPVR1[i-1,j]*n/5 else 0 ) 
	+ R1[i,j]*c*tp*SucR1t1ratio[j]*LmbdaW 
	- WPVR1[i,j]*(if i <n+1 then n/5 else 0)
	- WPVR1[i,j]*gWR1[j]
	- WPVR1[i,j]*m

d/dt (WPVR2[1..n+1,1..5]) = (if i > 1 then WPVR2[i-1,j]*n/5 else 0 ) 
	+ R2[i,j]*SucR2t1ratio[j]*c*tp*LmbdaW 
	- WPVR2[i,j]*(if i <n+1 then n/5 else 0)
	- WPVR2[i,j]*gWR2[j]
	- WPVR2[i,j]*m

d/dt (OPVR1[1..n+1,1..5]) = (if i > 1 then OPVR1[i-1,j]*n/5 else 0 ) 
	+ R1[i,j]*c*tp*SucR1t1ratio[j]*LmbdaO 
	+ R1[i,j]*(if i < n+1 then VaccRtbe*SucR1t1ratio[j] else 0) 
	- OPVR1[i,j]* (if i < n+1 then n/5 else 0) 
	- OPVR1[i,j]*gOR1[j]
	- OPVR1[i,j]*m

d/dt (OPVR2[1..n+1,1..5]) = (if i > 1 then OPVR2[i-1,j]*n/5 else 0 )  
	+ R2[i,j]*c*tp*SucR2t1ratio[j]*LmbdaO 
	+ R2[i,j]*(if i < n+1 then VaccRtbe*SucR2t1ratio[j] else 0) 
	- OPVR2[i,j]*(if i < n+1 then n/5 else 0)
	- OPVR2[i,j]*gOR2[j]
	- OPVR2[i,j]*m

  dS[0] = m*totPop - S[0]*con*tp*(LmbdaW +LmbdaO) - S[0] * ((n / 5.0) +m +VaccRtbe);
  for(int i=1; i<n; i++)
    dS[i] = S[i-1] * (n/5.0) - S[i]*con*tp*(LmbdaW +LmbdaO) - S[i] * ((n/5.0)+m+VaccRtbe);
  dS[n] = S[n-1] * (n/5.0) - S[n]*con*tp*(LmbdaW +LmbdaO) - S[n] * m;
  for(int i=0; i<n+1; i++) {
    dWPV1[i] = ( (i>0)*WPV1[i-1]*n/5.0 + S[i]*con*tp*LmbdaW - WPV1[i]*((m + gW1) + (i<n)* n/5.0) ) * (1-EradStatus[0]) ; // after WPV is eradicated, the prevalence stays at 0.
    dOPV1[i] = (i>0)*OPV1[i-1]*n/5.0 + (i<n)* S[i] * VaccRtbe + S[i]*con*tp*LmbdaO - OPV1[i]*(m+(i < n)* n/5.0 + gO1);
    dR[i] = (i >0)* R[i-1]*n/5.0 + WPV1[i] * gW1 + WPVR[i] * gWR + OPV1[i] * gO1 + OPVR[i] * gOR  - R[i] * WnRt - R[i] * ((i < n)* n/5.0 + m);
    dP1[i] = (i >0)* P1[i-1]*n/5.0 + R[i]*WnRt - P1[i]*con*SucRt1ratio1*tp*(LmbdaW + LmbdaO) - P1[i] * ((i < n)*n/5.0 + (i < n)*VaccRtbe*SucRt1ratio1 + WnRt1 + m);
    dWPVR[i] = ( (i >0)*WPVR[i-1]*n/5.0 + P1[i]*con*tp*SucRt1ratio1*LmbdaW + P2[i]*con*tp*SucRt1ratio2*LmbdaW + P3[i]*con*tp*SucRt1ratio3*LmbdaW + P4[i]*con*tp*SucRt1ratio4*LmbdaW - WPVR[i]*(m + (i<n)* n/5.0 + gWR) ) * (1-EradStatus[0]); // after WPV is eradicated, the prevalence stays at 0.
    dOPVR[i] = (i >0)*OPVR[i-1]*n/5.0 + P1[i]*(con*tp*LmbdaO + (i < n)*VaccRtbe)*SucRt1ratio1 + P2[i]*(con*tp*LmbdaO + (i < n)*VaccRtbe)*SucRt1ratio2 + P3[i]*(con*tp*LmbdaO + (i < n)*VaccRtbe)*SucRt1ratio3 + P4[i]*(con*tp*LmbdaO + (i < n)*VaccRtbe)*SucRt1ratio4 - OPVR[i]*(m + (i < n)*n/5.0+gOR);
    dP2[i] = (i >0)* P2[i-1]*n/5.0 + P1[i]*WnRt1 - P2[i]*con*SucRt1ratio2*tp*(LmbdaW + LmbdaO) - P2[i] * ((i < n)*n/5.0 + (i < n)*VaccRtbe*SucRt1ratio + WnRt2 + m);
    dP3[i] = (i >0)* P3[i-1]*n/5.0 + P2[i]*WnRt2 - P3[i]*con*SucRt1ratio3*tp*(LmbdaW + LmbdaO) - P3[i] * ((i < n)*n/5.0 + (i < n)*VaccRtbe*SucRt1ratio + WnRt3 + m);
    dP4[i] = (i >0)* P4[i-1]*n/5.0 + P3[i]*WnRt3 - P4[i]*con*SucRt1ratio4*tp*(LmbdaW + LmbdaO) - P4[i] * ((i < n)*n/5.0 + (i < n)*VaccRtbe*SucRt1ratio + m);
  }

  // Cumulative WPV incidences (NOT for one-year span, but the total from t=0.)
  dCumFrstWInf[0] = totS*con*tp*LmbdaW;
  dCumFrstFrRe[0] = totS*con*tp*totWPVR* RelContRt1 / totPop;
  dCumReFrRe[0] = (totP1*SucRt1ratio1+totP2*SucRt1ratio2+totP3*SucRt1ratio3+totP4*SucRt1ratio4)*con*tp*totWPVR* RelContRt1 / totPop;
  dCumFrstFrFrst[0] = totS*con*tp*totWPV1 / totPop;
  dCumReFrFrst[0] = (totP1*SucRt1ratio1+totP2*SucRt1ratio2+totP3*SucRt1ratio3+totP4*SucRt1ratio4)*con*tp*totWPV1 / totPop;
  dEradStatus[0] = 0.0;
}

/* WPV reintroduction events */
void reintro(int *nstate, double *t, double *y) 
{
  double* WPVR = y+2*(n+1);
  double* P1 = y+6*(n+1);
  WPVR[n] += 1.0;
  P1[n] -= 1.0;
}




/* root function */
// when totWPV equals ErLv, we will set WPV prevalence and its growth rate to 0.
// for this, we define an equation whose root corresponds to the event of totWPV reaching ErLv.
void eradroot(int *neq, double *t, double *y, int *ng, double *gout, double *out, int *ip)
{
  double totWPV = 0.0;
  for (int i=0; i<2*n+2; i++) {
    totWPV += y[n+1+i];
  }
  gout[0] = totWPV - ErLv;
}
 
/* event function */
void eradication(int *nstate, double *t, double *y)
{
  *(y+7*(n+1)+5) = 1.0; // EradStatus
  double* WPV1 = y+n+1;
  double* WPVR = y+2*(n+1);
  double* R = y+5*(n+1);
  for(int i = 0; i < n+1; i++) {
    R[i] += (WPV1[i] + WPVR[i]);
    WPV1[i] = 0.0;
    WPVR[i] = 0.0;
  }
}


/* END file odemod.c */
