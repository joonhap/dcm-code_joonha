require(ggplot2)
require(grid)
require(plyr) #for plotting subsets in ggplot

## generate aligned plots
wr = 0.02
wd_cube = 0.375
con = 260
InfPerAFP = 200
OvsW = 0.25

VRampTgrid <- seq(from= 0.4, to= 20, by=0.4); nvrt <- length(VRampTgrid)
VaccRbegrid <- seq(from= 0.1, to= 2.5, by=0.02); nvrb <- length(VaccRbegrid)

load(paste("RData/type_duration_con_", con, "_wr_", wr, "_wd_", wd_cube^(1/3),"_InfPerAFP_",InfPerAFP,"_OvsW_",OvsW, "_150613.RData",sep=""))

## mark VaccRbe values at which either Silent duration either starts or ceases to exceed 3 yrs.
Silent3yrStartEnd <- data.frame(NULL)
for(j.ptd in 1:nvrt) {
    Silent3yrStart <- 0; Silent3yrEnd <- 0
    for(k.ptd in 1:nvrb) {
        if(Silent3yrStart == 0 & CombinedResult[(j.ptd-1)*nvrb + k.ptd,"Silent3Dur"] > 3)
            Silent3yrStart <- k.ptd
        if(Silent3yrStart != 0 & k.ptd > Silent3yrStart & CombinedResult[(j.ptd-1)*nvrb + k.ptd,"Silent3Dur"] > 3)
            Silent3yrEnd <- k.ptd
    }
    if (Silent3yrStart != 0)
        Silent3yrStartEnd <- rbind(Silent3yrStartEnd, c(VaccRbegrid[Silent3yrStart], VRampTgrid[j.ptd]))
    if (Silent3yrEnd != 0)
        Silent3yrStartEnd <- rbind(Silent3yrStartEnd, c(VaccRbegrid[Silent3yrEnd], VRampTgrid[j.ptd]))
}
colnames(Silent3yrStartEnd) <- c("VaccRbe", "VRampT")

## mark a region where eradication happens earlier than the end of vaccination ramp-up
EarlyErad <- as.data.frame(CombinedResult[(CombinedResult[,"Erad3Tm"]<CombinedResult[,"VRampT"]), c("VaccRbe","VRampT")])

## stable eradication is marked in green
StableErad <- sapply(1:dim(CombinedResult)[1], function(x) all(CombinedResult[x:(((x-1)%/%nvrb+1)*nvrb),"StableErad"]==1)) ## since the simulation stops at finite time, in some cases (though very rare) unstable eradication can be marked as stable. So we need to check that all higher vaccination rates result in stable eradication.
CombinedResult[,"Silent3Type"] <- ifelse(StableErad==1 & CombinedResult[,"Silent3Type"]==1, 4, CombinedResult[,"Silent3Type"])
## we no longer distinguish the points where no second AFP nor eradication happens until simulation end time.
## instead of purple (as done previously), mark it in red
CombinedResult[,"Silent3Type"] <- ifelse(CombinedResult[,"Silent3Type"]==3, 2, CombinedResult[,"Silent3Type"])        

CombinedResult <- as.data.frame(CombinedResult)
## Eradication status / Silent circulation duration plot ###
## plot of eradication status and the duration of silent circulation
## the color is determined by the 'Silent3Type' column, and the intensity by the 'Silent3Dur'.
filename=paste("plots/type_duration_con_", con, "_wr_", wr, "_wd_", wd_cube^(1/3),"_InfPerAFP_",InfPerAFP,"_OvsW_",OvsW, "_150613.pdf",sep="")
pdf(filename, height=6, width=6)
p <- ggplot(data=CombinedResult, asp=1)
p <- p + geom_tile(aes(x=VaccRbe, y=VRampT, fill=factor(Silent3Type), alpha=pmin(8,pmax(0,Silent3Dur))))
p <- p + scale_fill_manual(values = c('0'='white','1'='blue','2'='red','4'='green'), breaks=c(0,1,2,4), guide=FALSE)
p <- p + scale_alpha(name="Silent duration",limits=c(0,8), guide=FALSE)
p <- p + theme(plot.margin=unit(c(5,10,0,0),"pt"), axis.ticks=element_blank(), panel.background=element_rect(fill="white"), axis.text=element_text(size=20), axis.title=element_text(size=22))
p <- p + labs(y="Vaccination ramp up time (years)") + labs(x=expression("Vaccination rate (yr"^{-1}*")")) 
p <- p + geom_point(data=Silent3yrStartEnd, aes(x= VaccRbe, y= VRampT), size=1.4, alpha=1)
p <- p + geom_point(data=EarlyErad, aes(x= VaccRbe, y= VRampT), shape=3, size=1.4, alpha=1)
print(p)
dev.off()
