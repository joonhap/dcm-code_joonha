### COMPUTE DERIVED VARIABLES ###
# starting indices for each compartments
IdxS = 1; IdxWPV1 = (n+1)+1; IdxWPVR = 2*(n+1)+1; IdxOPV1 = 3*(n+1)+1; IdxOPVR = 4*(n+1)+1; IdxR1 = 5*(n+1)+1; IdxR2 = 6*(n+1)+1;
SLt5 = apply(out[, IdxS+1:n], 1, sum)
S5p = out[, IdxS+n+1]
totS = SLt5 + S5p
WPV1Lt5 = apply(out[, IdxWPV1+1:n], 1, sum)
WPV15p = out[, IdxWPV1+n+1]
totWPV1 = WPV1Lt5 + WPV15p
WPVRLt5 = apply(out[, IdxWPVR+1:n], 1, sum)
WPVR5p = out[, IdxWPVR+n+1]
totWPVR = WPVRLt5 + WPVR5p
OPV1Lt5 = apply(out[, IdxOPV1+1:n], 1, sum)
OPV15p = out[, IdxOPV1+n+1]
totOPV1 = OPV1Lt5 + OPV15p
OPVRLt5 = apply(out[, IdxOPVR+1:n], 1, sum)
OPVR5p = out[, IdxOPVR+n+1]
totOPVR = OPVRLt5 + OPVR5p
R1Lt5 = apply(out[, IdxR1+1:n], 1, sum)
R15p = out[, IdxR1+n+1]
totR1 = R1Lt5 + R15p
R2Lt5 = apply(out[, IdxR2+1:n], 1, sum)
R25p = out[, IdxR2+n+1]
totR2 = R2Lt5 + R25p
totPop = totS + totWPV1 + totWPVR + totOPV1 + totOPVR + totR1 + totR2
totLt5 = SLt5 + WPV1Lt5 + WPVRLt5 + OPV1Lt5 + OPVRLt5 + R1Lt5 + R2Lt5
tot5p = totPop - totLt5

totWPV = totWPV1 + totWPVR
totOPV = totOPV1 + totOPVR
totInf = totWPV + totOPV
WPV5p = WPV15p + WPVR5p
OPV5p = OPV15p + OPVR5p
totErad3 = totWPV1 + totWPVR
totErad4 = totWPV1 + totWPVR * RelContRt1 * Rec1tRratio 
totErad5 = totWPV1 + totWPVR * (1 + RelContRt1 * Rec1tRratio) /2 

LmbdaW = (totWPV1 + totWPVR * RelContRt1) / totPop
LmbdaO = (totOPV1 + totOPVR * RelContRt1) * RelContOtW / totPop

# trace of the WPV growth rate matrix
aggWPVgrCoeff = (totS + totR2*SucRt1ratio*RelContRt1)*con*tp/totPop - (gW1+gWR+2*m)
# determinant of the WPV growth rate matrix
det = -gW1*totR2*RelContRt1*SucRt1ratio*con*tp/totPop - gWR*totS*con*tp/totPop + gW1*gWR
# R2 contribution to the determinant
detTerm1 = -gW1*totR2*RelContRt1*SucRt1ratio*con*tp/totPop
# S contribution to the determinant
detTerm2 = - gWR*totS*con*tp/totPop
# bigger eigenvalue of the rate matrix
biggerEigVal = .5*(aggWPVgrCoeff+sqrt(aggWPVgrCoeff^2 - 4*det))
# effective reproduction number for WPV reinfection
ReffRe = totR2*RelContRt1*SucRt1ratio*con*tp/totPop/(m+gWR)
# effective reproduction number for WPV first infection
ReffFrst = totS*con*tp/totPop / (m+gW1)
# effective reproduction number
ReffTot = ReffRe + ReffFrst

CumFrstWInf = out[ ,7*(n+1)+2];
CumFrstFrFrst = out[, 7*(n+1)+3];
CumFrstFrRe = out[, 7*(n+1)+4];
CumReFrFrst = out[, 7*(n+1)+5];
CumReFrRe = out[, 7*(n+1)+6];
CumFrstWInf1yr = CumFrstFrFrst1yr = CumFrstFrRe1yr = CumReFrFrst1yr = CumReFrRe1yr = rep(0,length(times));
tps1yr = round(1/(times[2]-times[1])) 
for (i.dv in 1:length(times)) {
    CumFrstWInf1yr[i.dv] = CumFrstWInf[i.dv] - ifelse(i.dv>tps1yr, CumFrstWInf[i.dv-tps1yr], 0)
    CumFrstFrFrst1yr[i.dv] = CumFrstFrFrst[i.dv] - ifelse(i.dv>tps1yr, CumFrstFrFrst[i.dv-tps1yr], 0)
    CumFrstFrRe1yr[i.dv] = CumFrstFrRe[i.dv] - ifelse(i.dv>tps1yr, CumFrstFrRe[i.dv-tps1yr], 0)
    CumReFrFrst1yr[i.dv] = CumReFrFrst[i.dv] - ifelse(i.dv>tps1yr, CumReFrFrst[i.dv-tps1yr], 0)
    CumReFrRe1yr[i.dv] = CumReFrRe[i.dv] - ifelse(i.dv>tps1yr, CumReFrRe[i.dv-tps1yr], 0)
}

# First AFP time is the first time CumFrstWInf1yr goes below InfPerAFP after VT
for (i.dv in 1:length(times)) 
    if (times[i.dv] >= VT & CumFrstWInf1yr[i.dv] < InfPerAFP) 
        break
IdxFrstAFPTm = i.dv
FrstAFPTm = times[i.dv]
# Second AFP time is the first time CumFrstWInf goes above InfPerAFP after FrstAFPTm
for (i.dv in IdxFrstAFPTm:length(times)) 
    if (CumFrstWInf[i.dv] - CumFrstWInf[IdxFrstAFPTm] >= InfPerAFP) 
        break
IdxScndAFPTm = i.dv
ScndAFPTm = times[i.dv]
# old, wrong way of computing Second AFP Time
#for (i.dv in 1:length(times)) 
#    if (times[i.dv] > FrstAFPTm & CumFrstWInf1yr[i.dv] >= InfPerAFP) 
#        break
#IdxScndAFPTm = i.dv
#ScndAFPTm = times[i.dv]
# Erad3Tm 
for (i.dv in 1:length(times)) 
    if (times[i.dv] > VT & totErad3[i.dv] < ErLv) 
        break
IdxErad3Tm = i.dv
Erad3Tm = times[i.dv]
# Erad4Tm
for (i.dv in 1:length(times)) 
    if (times[i.dv] > VT & totErad4[i.dv] < ErLv) 
        break
IdxErad4Tm = i.dv
Erad4Tm = times[i.dv]
# Erad5Tm
for (i.dv in 1:length(times)) 
    if (times[i.dv] > VT & totErad5[i.dv] < ErLv) 
        break
IdxErad5Tm = i.dv
Erad5Tm = times[i.dv]
# ResTm : Resurgence time as defined by r^{Tot} > 1
for (i.dv in 1:length(times))
    if (times[i.dv] > VT & ReffTot[i.dv] > 1)
        break
IdxResTm =i.dv
ResTm = times[i.dv]

# Eradication status: 0 - no case elmination, 1 - eradication, 2 - case comeback, 3 - could not be told
Silent3Type = ifelse(FrstAFPTm == maxT, 0, ifelse(Erad3Tm < ScndAFPTm, 1, ifelse(ScndAFPTm < maxT, 2, 3)))
Silent4Type = ifelse(FrstAFPTm == maxT, 0, ifelse(Erad4Tm < ScndAFPTm, 1, ifelse(ScndAFPTm < maxT, 2, 3)))
Silent5Type = ifelse(FrstAFPTm == maxT, 0, ifelse(Erad5Tm < ScndAFPTm, 1, ifelse(ScndAFPTm < maxT, 2, 3)))

Silent3Dur = ifelse(Silent3Type == 0, 0, ifelse(Silent3Type == 1, Erad3Tm - FrstAFPTm, ifelse(Silent3Type == 2, ScndAFPTm - FrstAFPTm, maxT - FrstAFPTm)))
Silent4Dur = ifelse(Silent4Type == 0, 0, ifelse(Silent4Type == 1, Erad4Tm - FrstAFPTm, ifelse(Silent4Type == 2, ScndAFPTm - FrstAFPTm, maxT - FrstAFPTm)))
Silent5Dur = ifelse(Silent5Type == 0, 0, ifelse(Silent5Type == 1, Erad5Tm - FrstAFPTm, ifelse(Silent5Type == 2, ScndAFPTm - FrstAFPTm, maxT - FrstAFPTm)))

WPVatFrstAFP = totWPV[IdxFrstAFPTm]

StableErad3 = ifelse(Erad3Tm == maxT, 2, ifelse(ResTm == maxT, 1, 0))
StableErad4 = ifelse(Erad4Tm == maxT, 2, ifelse(ResTm == maxT, 1, 0))
StableErad5 = ifelse(Erad5Tm == maxT, 2, ifelse(ResTm == maxT, 1, 0))

Stable3Dur = ifelse(StableErad3 == 2, 0, ResTm - Erad3Tm)
Stable4Dur = ifelse(StableErad4 == 2, 0, ResTm - Erad4Tm)
Stable5Dur = ifelse(StableErad5 == 2, 0, ResTm - Erad5Tm)

CumFrstFrFrst3Slnt = ifelse(Silent3Type == 0, 0, ifelse(Silent3Type == 1, CumFrstFrFrst[IdxErad3Tm] - CumFrstFrFrst[IdxFrstAFPTm], ifelse(Silent3Type == 2, CumFrstFrFrst[IdxScndAFPTm] - CumFrstFrFrst[IdxFrstAFPTm], CumFrstFrFrst[length(times)] - CumFrstFrFrst[IdxFrstAFPTm])))
CumFrstFrRe3Slnt = ifelse(Silent3Type == 0, 0, ifelse(Silent3Type == 1, CumFrstFrRe[IdxErad3Tm] - CumFrstFrRe[IdxFrstAFPTm], ifelse(Silent3Type == 2, CumFrstFrRe[IdxScndAFPTm] - CumFrstFrRe[IdxFrstAFPTm], CumFrstFrRe[length(times)] - CumFrstFrRe[IdxFrstAFPTm])))
CumReFrFrst3Slnt = ifelse(Silent3Type == 0, 0, ifelse(Silent3Type == 1, CumReFrFrst[IdxErad3Tm] - CumReFrFrst[IdxFrstAFPTm], ifelse(Silent3Type == 2, CumReFrFrst[IdxScndAFPTm] - CumReFrFrst[IdxFrstAFPTm], CumReFrFrst[length(times)] - CumReFrFrst[IdxFrstAFPTm])))
CumReFrRe3Slnt = ifelse(Silent3Type == 0, 0, ifelse(Silent3Type == 1, CumReFrRe[IdxErad3Tm] - CumReFrRe[IdxFrstAFPTm], ifelse(Silent3Type == 2, CumReFrRe[IdxScndAFPTm] - CumReFrRe[IdxFrstAFPTm], CumReFrRe[length(times)] - CumReFrRe[IdxFrstAFPTm])))
CumFrst3Slnt = CumFrstFrRe3Slnt + CumFrstFrFrst3Slnt 
CumRe3Slnt = CumReFrRe3Slnt + CumReFrFrst3Slnt
CumFrFrst3Slnt = CumFrstFrFrst3Slnt + CumReFrFrst3Slnt 
CumFrRe3Slnt = CumFrstFrRe3Slnt + CumReFrRe3Slnt 

CumFrstFrFrst4Slnt = ifelse(Silent4Type == 0, 0, ifelse(Silent4Type == 1, CumFrstFrFrst[IdxErad4Tm] - CumFrstFrFrst[IdxFrstAFPTm], ifelse(Silent4Type == 2, CumFrstFrFrst[IdxScndAFPTm] - CumFrstFrFrst[IdxFrstAFPTm], CumFrstFrFrst[length(times)] - CumFrstFrFrst[IdxFrstAFPTm])))
CumFrstFrRe4Slnt = ifelse(Silent4Type == 0, 0, ifelse(Silent4Type == 1, CumFrstFrRe[IdxErad4Tm] - CumFrstFrRe[IdxFrstAFPTm], ifelse(Silent4Type == 2, CumFrstFrRe[IdxScndAFPTm] - CumFrstFrRe[IdxFrstAFPTm], CumFrstFrRe[length(times)] - CumFrstFrRe[IdxFrstAFPTm])))
CumReFrFrst4Slnt = ifelse(Silent4Type == 0, 0, ifelse(Silent4Type == 1, CumReFrFrst[IdxErad4Tm] - CumReFrFrst[IdxFrstAFPTm], ifelse(Silent4Type == 2, CumReFrFrst[IdxScndAFPTm] - CumReFrFrst[IdxFrstAFPTm], CumReFrFrst[length(times)] - CumReFrFrst[IdxFrstAFPTm])))
CumReFrRe4Slnt = ifelse(Silent4Type == 0, 0, ifelse(Silent4Type == 1, CumReFrRe[IdxErad4Tm] - CumReFrRe[IdxFrstAFPTm], ifelse(Silent4Type == 2, CumReFrRe[IdxScndAFPTm] - CumReFrRe[IdxFrstAFPTm], CumReFrRe[length(times)] - CumReFrRe[IdxFrstAFPTm])))
CumFrst4Slnt = CumFrstFrRe4Slnt + CumFrstFrFrst4Slnt 
CumRe4Slnt = CumReFrRe4Slnt + CumReFrFrst4Slnt
CumFrFrst4Slnt = CumFrstFrFrst4Slnt + CumReFrFrst4Slnt 
CumFrRe4Slnt = CumFrstFrRe4Slnt + CumReFrRe4Slnt 

CumFrstFrFrst5Slnt = ifelse(Silent5Type == 0, 0, ifelse(Silent5Type == 1, CumFrstFrFrst[IdxErad5Tm] - CumFrstFrFrst[IdxFrstAFPTm], ifelse(Silent5Type == 2, CumFrstFrFrst[IdxScndAFPTm] - CumFrstFrFrst[IdxFrstAFPTm], CumFrstFrFrst[length(times)] - CumFrstFrFrst[IdxFrstAFPTm])))
CumFrstFrRe5Slnt = ifelse(Silent5Type == 0, 0, ifelse(Silent5Type == 1, CumFrstFrRe[IdxErad5Tm] - CumFrstFrRe[IdxFrstAFPTm], ifelse(Silent5Type == 2, CumFrstFrRe[IdxScndAFPTm] - CumFrstFrRe[IdxFrstAFPTm], CumFrstFrRe[length(times)] - CumFrstFrRe[IdxFrstAFPTm])))
CumReFrFrst5Slnt = ifelse(Silent5Type == 0, 0, ifelse(Silent5Type == 1, CumReFrFrst[IdxErad5Tm] - CumReFrFrst[IdxFrstAFPTm], ifelse(Silent5Type == 2, CumReFrFrst[IdxScndAFPTm] - CumReFrFrst[IdxFrstAFPTm], CumReFrFrst[length(times)] - CumReFrFrst[IdxFrstAFPTm])))
CumReFrRe5Slnt = ifelse(Silent5Type == 0, 0, ifelse(Silent5Type == 1, CumReFrRe[IdxErad5Tm] - CumReFrRe[IdxFrstAFPTm], ifelse(Silent5Type == 2, CumReFrRe[IdxScndAFPTm] - CumReFrRe[IdxFrstAFPTm], CumReFrRe[length(times)] - CumReFrRe[IdxFrstAFPTm])))
CumFrst5Slnt = CumFrstFrRe5Slnt + CumFrstFrFrst5Slnt 
CumRe5Slnt = CumReFrRe5Slnt + CumReFrFrst5Slnt
CumFrFrst5Slnt = CumFrstFrFrst5Slnt + CumReFrFrst5Slnt 
CumFrRe5Slnt = CumFrstFrRe5Slnt + CumReFrRe5Slnt 
